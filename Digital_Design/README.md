# Digital design of a QPSK basic transceiver 

This project contains the develop of a basic QPSK transceiver. This directory contains the project stages

### [Fixed Point](Fixed_Point/)
- Simulations of the digital transceiver on Python.
- Analysis of penalty resulting from represent filter taps using fixed point insted of floating point.
- Verilog descriptions of fixed point adder and multiplier.

### [Basic Comm System Implementation](Basic_Com_System_Implementation/)
- Simulations of the implemented architecture on Python
- Verilog descriptions of the system.
- Simulations of the HDL on Vivado.

### [MicroBlaze test](MicroBlaze_test/)
- First use of MicroBlaze Xilinx IP (Vivado).
- Develop of microcontroller firmware using Vitis.
- Use of VIO Xilinx IP for system monitoring.

### [Comm System controlled by MicroBlaze](Comm_System_and_microBlaze/)
- [Integration](Comm_System_and_microBlaze/) of the previously [designed transceiver](Basic_Com_System_Implementation/) with MicroBlaze and VIO instanciations (Vitis and Vivado).
- New transceiver features:
  - Block RAM (storage of transmitted signal)
  - Register file description (used for system control)
- Control-interface for user on Python.
- Implementation on Artix-7 35T Evaluation Kit (Arty7 FPGA).
