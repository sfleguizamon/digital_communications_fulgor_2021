/*
 * .SYNOPSIS
 * This program controls GPOs of MicroBlaze microcontroller depending on the data received 
 * through UART. It also reads GPIs status which are used to read data from system memory. GPIs
 * status is send through UART. 
 * .DESCRIPTION
 * Received frame is 4 bytes in size, and it's composed as it follows:
 * | command | enable | data |
 * Command: 8-bits long.
 * Enable: 1-bit.
 * Data: 23-bits long.
 * Received frame is written directly in the GPOs three times, toggling enable bit. This is because
 * register file detects a new frame by detecting positive edges in enable bit.
 * GPIs are used to read data from register file (which is connected to system memory or ber related
 * registers). GPIs status is send through UART when is required by input commands.
 * .PARAMETER <Parameter_Name>
 * None
 * .INPUTS
 * None
 * .OUTPUTS
 * None
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, September 10th 2021, 12:20:43 pm
 *    File: protocolo_main.c
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 * 
 */

#include <stdio.h>
#include <string.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xgpio.h"
#include "platform.h"
#include "xuartlite.h"
#include "microblaze_sleep.h"

#define PORT_IN	 		XPAR_AXI_GPIO_0_DEVICE_ID //XPAR_GPIO_0_DEVICE_ID
#define PORT_OUT 		XPAR_AXI_GPIO_0_DEVICE_ID //XPAR_GPIO_0_DEVICE_ID

/* Feedback requirement commands */
#define def_LOG_READ	0x35000000
#define def_BER_READ	0x40000000

XGpio GpioOutput;
XGpio GpioInput;
XUartLite uart_module;

int main()
{
  init_platform();

  int Status;
    u32 GPIOvalue;
    u8 sdata[4];
    u8 rdata[4];
    u16 leds_status;
    u32 receivedFrame;

  /* Peripherals initialization */

  XUartLite_Initialize(&uart_module, 0);

  Status=XGpio_Initialize(&GpioInput, PORT_IN);
  if(Status!=XST_SUCCESS){
      return XST_FAILURE;
    }

  Status=XGpio_Initialize(&GpioOutput, PORT_OUT);
  if(Status!=XST_SUCCESS){
    return XST_FAILURE;
  }

  XGpio_SetDataDirection(&GpioOutput, 1, 0x00000000); // output GPIO
  XGpio_SetDataDirection(&GpioInput, 1, 0xFFFFFFFF);	// input GPIO

  /* Super Loop */
  while(1){

    /* Reading UART */
    read(stdin,&rdata[0],4);

    /* Lining up frame */
    receivedFrame = rdata[0]<<24 | rdata[1]<<16 | rdata[2]<<8 | rdata[3];

    /* Writing GPIO and toggling 'enable' bit */
    XGpio_DiscreteWrite(&GpioOutput,1, (u32) receivedFrame);
    XGpio_DiscreteWrite(&GpioOutput,1, (u32) receivedFrame | 0x00800000); 
    XGpio_DiscreteWrite(&GpioOutput,1, (u32) receivedFrame & 0xFF7FFFFF);

    /* Reading GPIO and sending through UART when received command requires feedback */
    if(receivedFrame == def_LOG_READ || (receivedFrame & 0xF0000000) == def_BER_READ){
      GPIOvalue = XGpio_DiscreteRead(&GpioInput, 1);
      sdata[0]=GPIOvalue >> 24;
      sdata[1]=GPIOvalue >> 16;
      sdata[2]=GPIOvalue >> 8;
      sdata[3]=GPIOvalue;
      while(XUartLite_IsSending(&uart_module)){}
      XUartLite_Send(&uart_module, &(sdata[0]),4);
    }
  }
  
  cleanup_platform();
  return 0;
}
