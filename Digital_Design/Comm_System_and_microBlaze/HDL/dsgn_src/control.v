/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Tuesday, August 10th 2021, 8:24:27 pm
 *    File: control.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */

module control #(
  parameter os_rate   = 4     ,
  parameter MAPPER_EN = 1'b1

) (
  input       i_clock         ,
  input       i_reset         ,
  output reg  o_PRBS9_enable  ,
  output      o_mapper_enable
) ;

  localparam COUNTER_SIZE = $clog2(os_rate) ;

  reg [COUNTER_SIZE - 1 : 0]  counter ;

  /*
    Since a single clock signal is used, PRBS output is 
    enabled once every 'os_rate' clock cycles.      
  */

  always @(posedge i_clock or posedge i_reset ) begin
    if (i_reset) begin
      counter <= {COUNTER_SIZE{1'b0}} ;
    end
    else begin
      counter <= counter + 1  ;
    end
  end

  always @(*) begin
    if (counter == {COUNTER_SIZE{1'b0}}) begin
      o_PRBS9_enable = 1  ;
    end
    else begin
      o_PRBS9_enable = 0  ;
    end
  end

  assign o_mapper_enable = MAPPER_EN;
    
endmodule