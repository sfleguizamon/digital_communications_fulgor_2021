/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, September 10th 2021, 09:46:53 am
 *    File: top.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */

module top #(
  /* ARTIX-7 35 PARAMETERS */
  parameter NB_LEDS       = 4               ,
  parameter NB_RGB        = 3               ,
  
  /* MICROBLAZE PARAMETERS */
  parameter NB_GPIOS      = 32              ,

  /* TRANSCEIVER PARAMETERS */
  /* -- Tx data -- */
  parameter PRBS9_seed_I  = 9'b101010101    ,
  parameter PRBS9_seed_Q  = 9'b011111111    ,
  /* - Tx filter - */
  parameter N_TAPS        = 24              ,
  parameter NB_TAPS       = 8               ,
  parameter NBF_TAPS      = 7               ,
  parameter os_rate       = 4               ,
  parameter MAPPER_EN     = 1
)
(
  output [ NB_RGB - 1 : 0  ]    o_leds_rgb0 ,
  output [ NB_RGB - 1 : 0  ]    o_leds_rgb1 ,
  output [ NB_RGB - 1 : 0  ]    o_leds_rgb2 ,
  output [ NB_RGB - 1 : 0  ]    o_leds_rgb3 ,
  output [ NB_LEDS - 1 : 0 ]    o_leds      ,

  output                        o_tx_uart   ,
  input                         i_rx_uart   ,

  input                         i_reset     ,
  input                         i_clk100
) ;

/* LOCAL PARAMETERS ------------------------------------------------------- */
  /* ----- Tx ---- */
  localparam NB_TX_OUT        = (2+NB_TAPS) + (N_TAPS/os_rate - 1)  ;
  localparam NB_CHECKER_DATA  = 64            ;
  /* --- Frame --- */
  localparam NB_CTRL_FRAME    = 32            ;
  localparam NB_FRAME_CMD     = 8             ;
  localparam NB_FRAME_EN      = 1             ;
  localparam NB_FRAME_DATA    = 23            ;
  /* --- Memory --- */
  localparam NB_I_MEM_DATA    = 2 * NB_TX_OUT ;
  localparam NB_O_DATA_BUS    = 32            ;
  localparam NB_MEM_ADDR      = 12            ;

/* VARIABLES -------------------------------------------------------------- */
  /* ----- TxRx ---- */
  wire  [ NB_TX_OUT - 1 : 0       ] tx_out_I              ;
  wire  [ NB_TX_OUT - 1 : 0       ] tx_out_Q              ;
  wire                              PRBS9_enable          ;
  wire                              mapper_en             ;
  wire                              PRBS9_out_I           ;
  wire                              PRBS9_out_Q           ;
  wire                              slicer_out_I          ;
  wire                              slicer_out_Q          ;
  wire                              symb_I                ;
  wire                              symb_Q                ;
  wire                              checker_I             ; // prbs_checker to led
  wire                              checker_Q             ; // prbs_checker to led
  wire  [ NB_CHECKER_DATA - 1 :0  ] errors_count_I        ; // prbs_checker outs to reg file
  wire  [ NB_CHECKER_DATA - 1 :0  ] errors_count_Q        ; // prbs_checker outs to reg file
  wire  [ NB_CHECKER_DATA - 1 :0  ] bits_count_I          ; // prbs_checker outs to reg file
  wire  [ NB_CHECKER_DATA - 1 :0  ] bits_count_Q          ; // prbs_checker outs to reg file
  /* MicroBlaze */
  wire                              locked                ;
  wire                              clockdsp              ;
  wire  [ NB_GPIOS - 1 : 0        ] gpo0                  ; // microBlaze to reg file
  wire  [ NB_GPIOS - 1 : 0        ] gpi0                  ; // reg file to microBlaze
  /* Reg file */
  wire                              rst_from_rf           ; // reg file to syst and o_led[0]
  wire                              tx_en_from_rf         ; // 
  wire                              rx_en_from_rf         ; //
  wire  [ 1 : 0                   ] phase_sel_from_rf     ; //
  wire                              rf_run_mem_log        ;
  wire                              rf_read_mem_log       ;
  wire  [ NB_MEM_ADDR - 1 : 0     ] rf_read_addr_mem_log  ;
  /* Memory log */
  wire                              mem_full              ;
  wire  [ NB_I_MEM_DATA - 1 : 0   ] mem_o_data_to_rf      ;

/* REGISTER FILE (RF) ----------------------------------------------------- */
  reg_file #(
    .NB_CTRL_FRAME        ( NB_CTRL_FRAME         ) ,
    .NB_FRAME_CMD         ( NB_FRAME_CMD          ) ,
    .NB_FRAME_EN          ( NB_FRAME_EN           ) ,
    .NB_FRAME_DATA        ( NB_FRAME_DATA         ) ,
    .NB_I_DATA_TX         ( NB_I_MEM_DATA         ) ,
    .NB_MEM_ADDR          ( NB_MEM_ADDR           )
  )
  u_reg_file (
    .i_clock              ( i_clk100              ) ,
    .i_reset              ( ~i_reset              ) ,
    .i_frame              ( gpo0                  ) ,
    .i_data_log_from_mem  ( mem_o_data_to_rf      ) , 
    .i_memfull            ( mem_full              ) ,
    .i_ber_samp_I         ( bits_count_I          ) ,
    .i_ber_samp_Q         ( bits_count_Q          ) ,
    .i_ber_error_I        ( errors_count_I        ) ,
    .i_ber_error_Q        ( errors_count_Q        ) ,
    .o_data               ( gpi0                  ) , 
    .o_rst                ( rst_from_rf           ) ,
    .o_tx_en              ( tx_en_from_rf         ) ,
    .o_rx_en              ( rx_en_from_rf         ) ,
    .o_phase_sel          ( phase_sel_from_rf     ) ,
    .o_run_log            ( rf_run_mem_log        ) ,
    .o_read_log           ( rf_read_mem_log       ) ,
    .o_addr_log_to_mem    ( rf_read_addr_mem_log  )
  ) ;

/* MICROBLAZE ------------------------------------------------------------- */
  MicroGPIO
  u_micro (
    .clock100           ( clockdsp  ) ,  // Clock de aplicacion
    .gpio_rtl_tri_o     ( gpo0      ) ,  // GPIO output
    .gpio_rtl_tri_i     ( gpi0      ) ,  // GPIO input
    .reset              ( i_reset   ) ,  // Hard Reset
    .sys_clock          ( i_clk100  ) ,  // Clock de FPGA
    .o_lock_clock       ( locked    ) ,  // Salida Lock Clock
    .usb_uart_rxd       ( i_rx_uart ) ,  // UART RX
    .usb_uart_txd       ( o_tx_uart )    // UART TX
  );


/* CONTROL (SYNCRONIZATION) MODULE ---------------------------------------- */
  control #(
    .os_rate          ( os_rate       ) ,
    .MAPPER_EN        ( MAPPER_EN     )
  )
  u_control (
    .i_clock          ( i_clk100      ) ,
    .i_reset          ( rst_from_rf   ) ,
    .o_PRBS9_enable   ( PRBS9_enable  ) ,
    .o_mapper_enable  ( mapper_en     )
  ) ;

  
/* IN-PHASE SIGNAL RELATED PATH ARCHITECTURE ------------------------------ */
  PRBS9 #(
    .seed     ( PRBS9_seed_I                  )
  )
  u_PRBS9_I (
    .i_clock  ( i_clk100                      ) ,
    .i_reset  ( rst_from_rf                   ) ,
    .i_enable ( PRBS9_enable & tx_en_from_rf  ) ,
    .o_PRBS9  ( PRBS9_out_I                   )
  ) ;

  FIR_Filter #(
    .N_TAPS           ( N_TAPS        ) ,
    .NB_TAPS          ( NB_TAPS       ) ,
    .NBF_TAPS         ( NBF_TAPS      ) ,
    .os_rate          ( os_rate       )
  )
  u_FIR_Filter_I (
    .i_clock          ( i_clk100      ) ,
    .i_reset          ( rst_from_rf   ) ,
    .i_enable         ( tx_en_from_rf ) ,
    .i_signal_bit_in  ( PRBS9_out_I   ) ,
    .i_mapper_en      ( mapper_en     ) ,
    .o_conv           ( tx_out_I      )
  ) ;

  slicer #()
  u_slicer_I (
    .i_sample_sign  ( tx_out_I[NB_TX_OUT-1] ) ,
    .o_bit          ( slicer_out_I          )
  ) ;

  downsampler #()
  u_downsampler_I (
    .i_sample       ( slicer_out_I      ) ,
    .i_clock        ( i_clk100          ) ,
    .i_reset        ( rst_from_rf       ) ,
    .i_enable       ( PRBS9_enable      ) ,
    .i_phase_select ( phase_sel_from_rf ) ,
    .o_symb         ( symb_I            )
  ) ;

  prbs_checker #(
    .NB_DATA_OUTPUT ( NB_CHECKER_DATA )
  )
  u_prbs_checker_I (
    .i_clock        ( i_clk100        ) ,
    .i_reset        ( ~rst_from_rf    ) ,
    .i_enable       ( rx_en_from_rf   ) ,
    .i_ref_bit      ( PRBS9_out_I     ) ,
    .i_bit          ( symb_I          ) ,
    .o_led          ( checker_I       ) ,
    .o_errors_count ( errors_count_I  ) ,
    .o_bits_count   ( bits_count_I    )
  ) ;

/* IN-QUADRATURE SIGNAL RELATED PATH ARCHITECTURE ------------------------- */
  PRBS9 #(
    .seed         ( PRBS9_seed_Q                  )
  )
  u_PRBS9_Q (
    .i_clock      ( i_clk100                      ) ,
    .i_reset      ( rst_from_rf                   ) ,
    .i_enable     ( PRBS9_enable & tx_en_from_rf  ) ,
    .o_PRBS9      ( PRBS9_out_Q                   )
  ) ;

  FIR_Filter #(
    .N_TAPS           ( N_TAPS        ) ,
    .NB_TAPS          ( NB_TAPS       ) ,
    .NBF_TAPS         ( NBF_TAPS      ) ,
    .os_rate          ( os_rate       )
  )
  u_FIR_Filter_Q (
    .i_clock          ( i_clk100      ) ,
    .i_reset          ( rst_from_rf   ) ,
    .i_enable         ( tx_en_from_rf ) ,
    .i_signal_bit_in  ( PRBS9_out_Q   ) ,
    .i_mapper_en      ( mapper_en     ) ,
    .o_conv           ( tx_out_Q      )
  ) ;

  slicer #()
  u_slicer_Q (
    .i_sample_sign  ( tx_out_Q[NB_TX_OUT-1] ) ,
    .o_bit          ( slicer_out_Q          )
  ) ;

  downsampler #()
  u_downsampler_Q (
    .i_sample       ( slicer_out_Q          ) ,
    .i_clock        ( i_clk100              ) ,
    .i_reset        ( rst_from_rf           ) ,
    .i_enable       ( PRBS9_enable          ) ,
    .i_phase_select ( phase_sel_from_rf     ) ,
    .o_symb         ( symb_Q                )
  ) ;

  prbs_checker #(
    .NB_DATA_OUTPUT ( NB_CHECKER_DATA )
  )
  u_prbs_checker_Q (
    .i_clock        ( i_clk100        ) ,
    .i_reset        ( ~rst_from_rf    ) ,
    .i_enable       ( rx_en_from_rf   ) ,
    .i_ref_bit      ( PRBS9_out_Q     ) ,
    .i_bit          ( symb_Q          ) ,
    .o_led          ( checker_Q       ) ,
    .o_errors_count ( errors_count_Q  ) ,
    .o_bits_count   ( bits_count_Q    )
  ) ;
  
/* MEMORY CONTROL --------------------------------------------------------- */
  mem_control #(
    .NB_ADDRESS(NB_MEM_ADDR),
    .NB_DATA(NB_I_MEM_DATA)
  )
  u_mem_control (    
    .i_clock            ( i_clk100              ) , // FIXME: usando clockdsp no cerraba timing
    .i_data             ( {tx_out_I,tx_out_Q}   ) ,
    .i_rf_read_address  ( rf_read_addr_mem_log  ) ,
    .i_rf_reset_mem     ( rst_from_rf           ) ,
    .i_rf_load_mem_en   ( rf_run_mem_log        ) ,
    .o_rf_mem_full      ( mem_full              ) ,
    .o_rf_data          ( mem_o_data_to_rf      )
  );

/* VIRTUAL INPUT/OUTPUT (VIO) --------------------------------------------- */
  vio
  u_vio (
    .clk_0        ( clockdsp                            ) ,
    .probe_in0_0  ( {rf_read_mem_log, mem_full, locked} ) ,
    .probe_in1_0  ( {phase_sel_from_rf, rf_run_mem_log} ) ,
    .probe_in2_0  ( rst_from_rf                         ) ,
    .probe_in3_0  ( tx_en_from_rf                       ) ,
    .probe_in4_0  ( rx_en_from_rf                       ) ,
    .probe_in5_0  ( checker_I & checker_Q               )
  ) ;
  
/* LEDS WIRING ------------------------------------------------------------ */
  assign o_leds[0] = rst_from_rf            ;
  assign o_leds[1] = tx_en_from_rf          ;
  assign o_leds[2] = rx_en_from_rf          ;
  assign o_leds[3] = checker_I & checker_Q  ;

  assign o_leds_rgb0[0] = locked            ;
  assign o_leds_rgb0[1] = mem_full          ;
  assign o_leds_rgb0[2] = rf_read_mem_log   ;

  assign o_leds_rgb1[0] = rf_run_mem_log    ;
  // assign o_leds_rgb1[1] = gpo0[4];
  // assign o_leds_rgb1[2] = gpo0[5];

  // assign o_leds_rgb2[0] = gpo0[6];
  // assign o_leds_rgb2[1] = gpo0[7];
  // assign o_leds_rgb2[2] = gpo0[8];

  // assign o_leds_rgb3[0] = gpo0[9];
  // assign o_leds_rgb3[1] = gpo0[10];
  // assign o_leds_rgb3[2] = gpo0[11];

endmodule