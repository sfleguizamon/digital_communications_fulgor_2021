module posedge_detector (
    input   i_signal  ,
    input   i_clock     ,
    output  o_posedge
);

reg d_signal ;

always @(posedge i_clock) begin
  d_signal <= i_signal ;
end

assign o_posedge = i_signal & ~d_signal ;
    
endmodule