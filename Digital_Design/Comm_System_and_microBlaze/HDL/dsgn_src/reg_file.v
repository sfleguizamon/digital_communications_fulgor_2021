/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, September 10th 2021, 11:32:07 am
 *    File: reg_file.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */

module reg_file #(
  /* FRAME PARAMETERS */
  parameter NB_CTRL_FRAME = 32  ,
  parameter NB_FRAME_CMD  = 8   ,
  parameter NB_FRAME_EN   = 1   ,
  parameter NB_FRAME_DATA = 23  ,

  /* MEMORY PARAMETERS */
  parameter NB_I_BER_DATA = 64  ,
  parameter NB_O_DATA_BUS = 32  ,
  parameter NB_I_DATA_TX  = 20  ,
  parameter NB_MEM_ADDR   = 12
)(
  input                                 i_clock             ,
  input                                 i_reset             ,
  input       [ NB_CTRL_FRAME - 1 : 0 ] i_frame             ,
  input       [ NB_I_DATA_TX - 1 : 0  ] i_data_log_from_mem ,
  input                                 i_memfull           ,
  input       [ NB_I_BER_DATA - 1 : 0 ] i_ber_samp_I        ,
  input       [ NB_I_BER_DATA - 1 : 0 ] i_ber_samp_Q        ,
  input       [ NB_I_BER_DATA - 1 : 0 ] i_ber_error_I       ,
  input       [ NB_I_BER_DATA - 1 : 0 ] i_ber_error_Q       ,
  output reg  [ NB_O_DATA_BUS - 1 : 0 ] o_data              ,
  output reg                            o_rst               ,
  output reg                            o_tx_en             ,
  output reg                            o_rx_en             ,
  output reg  [ 1 : 0                 ] o_phase_sel         ,
  output reg                            o_run_log           ,
  output reg                            o_read_log          ,
  output reg  [ NB_MEM_ADDR - 1 : 0   ] o_addr_log_to_mem
);

/* COMMAND OPTIONS */
localparam TX_CMD     = 4'h1  ;
localparam RX_CMD     = 4'h2  ;
localparam LOG_CMD    = 4'h3  ;
localparam BER_CMD    = 4'h4  ;
localparam RST_CMD    = 4'hF  ;

/* COMMAND SUBPOTIONS */
localparam DISABLE    = 4'h0  ;
localparam ENABLE     = 4'h1  ;
localparam PHASE      = 4'h2  ;
localparam RUN        = 4'h3  ;
localparam STOP       = 4'h4  ;
localparam READ       = 4'h5  ;
localparam ADDR       = 4'h6  ;
localparam BER_I      = 4'h7  ; // total of samples
localparam BER_Q      = 4'h8  ; // total of samples
localparam ERR_BITS_I = 4'h9  ; // total of errors
localparam ERR_BITS_Q = 4'hA  ; // total of errors

localparam HIGH       = 4'h0  ; // high parts of ber and error bits 
localparam LOW        = 4'h1  ; // low parts of ber and error bits 

wire                            reg_w_en    ;
wire  [ NB_FRAME_CMD - 1 : 0  ] cmd_frame   ;
wire  [ NB_FRAME_DATA - 1 : 0 ] data_frame  ;
wire  [ NB_FRAME_EN - 1 : 0   ] frame_en    ;

assign cmd_frame  = i_frame [ NB_CTRL_FRAME-1 -: NB_FRAME_CMD                              ] ;
assign data_frame = i_frame [ (NB_CTRL_FRAME-NB_FRAME_CMD-NB_FRAME_EN)-1 -: NB_FRAME_DATA  ] ;
assign frame_en   = i_frame [ (NB_CTRL_FRAME-NB_FRAME_CMD)-1 -: NB_FRAME_EN                ] ;

posedge_detector
  u_posedge_detector(
    .i_clock    ( i_clock   ) ,
    .i_signal   ( frame_en  ) ,
    .o_posedge  ( reg_w_en  )
  );

always @(posedge i_clock or posedge i_reset) begin
  if (i_reset) begin
    o_tx_en           <= 1'b0 ;
    o_rx_en           <= 1'b0 ;
    o_phase_sel       <= 2'b0 ;
    o_run_log         <= 1'b0 ;
    o_read_log        <= 1'b0 ;
    o_addr_log_to_mem <= 0    ;
    o_rst             <= 1'b0 ;
    o_data            <= 0    ;
  end
  else begin  
    if(reg_w_en) begin
      case (cmd_frame[NB_FRAME_CMD-1 -: 4])
        /* Tx commands */
        TX_CMD: begin
          case (cmd_frame[NB_FRAME_CMD-4-1 -: 4])
            DISABLE: begin
              o_tx_en <= 1'b0;
            end
            ENABLE: begin
              o_tx_en <= 1'b1;
            end
          endcase
        end
        /* Rx commands */
        RX_CMD: begin
          case (cmd_frame[NB_FRAME_CMD-4-1 -: 4])
            DISABLE: begin
              o_rx_en <= 1'b0 ;
            end
            ENABLE: begin
              o_rx_en <= 1'b1 ;
            end
            PHASE: begin
              o_phase_sel <= data_frame ;
            end
          endcase
        end
        /* Log commands */
        LOG_CMD: begin
          case (cmd_frame[NB_FRAME_CMD-4-1 -: 4])
            RUN: begin
              o_run_log   <= 1'b1 ;
            end
            STOP: begin
              o_run_log   <= 1'b0 ;
            end
            READ: begin
              o_read_log  <= 1'b1 ;
              o_data      <= i_data_log_from_mem ;
            end
            ADDR: begin
              o_addr_log_to_mem <= data_frame ;
            end
          endcase
        end
        /* BER log commands */
        BER_CMD: begin
          o_read_log  <= 1'b0 ;
          case (cmd_frame[NB_FRAME_CMD-4-1 -: 4])
            BER_I: begin
              case (data_frame[3:0])
                HIGH: begin
                  o_data <= i_ber_samp_I[63:32] ;
                end
                LOW: begin
                  o_data <= i_ber_samp_I[31:0] ;
                end
              endcase
            end
            BER_Q: begin
              case (data_frame[3:0])
                HIGH: begin
                  o_data <= i_ber_samp_Q[63:32] ;
                end
                LOW: begin
                  o_data <= i_ber_samp_Q[31:0] ;
                end
              endcase
            end
            ERR_BITS_I: begin
              case (data_frame[3:0])
                HIGH: begin
                  o_data <= i_ber_error_I[63:32] ;
                end
                LOW: begin
                  o_data <= i_ber_error_I[31:0] ;
                end
              endcase
            end
            ERR_BITS_Q: begin
              case (data_frame[3:0])
                HIGH: begin
                  o_data <= i_ber_error_Q[63:32] ;
                end
                LOW: begin
                  o_data <= i_ber_error_Q[31:0] ;
                end
              endcase
            end
          endcase
        end
        /* Reset commands */
        RST_CMD: begin
          case (cmd_frame[NB_FRAME_CMD-4-1 -: 4])
            DISABLE: begin
              o_rst <= 1'b0 ;
            end
            ENABLE: begin
              o_rst <= 1'b1 ;
            end
          endcase
        end
      endcase
    end
  end
end

endmodule