/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Monday, August 9th 2021, 4:01:47 pm
 *    File: downsampler.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */

module downsampler #(
  parameter os_rate = 4 // oversampling rate
) (
  input                             i_clock         ,
  input                             i_reset         ,
  input                             i_sample        ,
  input                             i_enable        ,
  input       [$clog2(os_rate)-1:0] i_phase_select  ,
  output reg                        o_symb
);

  reg [os_rate-2:0] shiftreg  ;

  always @(posedge i_clock or posedge i_reset) begin
    if (i_reset) begin
      shiftreg  <= {(os_rate-1){1'b0}}  ;
      o_symb    <= 1'b0             ;
    end
    else begin
      shiftreg <= {shiftreg[os_rate-2-1:0], i_sample} ;
      if (i_enable == 1'b1) begin
        case (i_phase_select)
          2'b00: o_symb <= i_sample     ;
          2'b01: o_symb <= shiftreg[0]  ;
          2'b10: o_symb <= shiftreg[1]  ;
          2'b11: o_symb <= shiftreg[2]  ; 
        endcase
      end
    end
  end

endmodule