/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Wednesday, August 4th 2021, 5:28:16 pm
 *    File: FIR_Filter.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */

// Polyphasic decomposed FIR filter

module FIR_Filter #(
  parameter N_TAPS    = 24  , // Number of filter taps
  parameter NB_TAPS   = 8   , // Num of bits used in fixed point representation
  parameter NBF_TAPS  = 7   , // Num of fracional bits used in fxd point
  parameter os_rate   = 4     // Oversampling rate. Must be even
) (
  input                                                         i_clock         ,
  input                                                         i_reset         ,
  input                                                         i_enable        ,
  input                                                         i_signal_bit_in ,
  input                                                         i_mapper_en     ,
  output signed [ (2+NB_TAPS) + (N_TAPS/os_rate - 1) - 1 : 0 ]  o_conv
  /* o_conv size is calculated considering necessary num of bits for multiplications and adds */
);

/* LOCAL PARAMETERS ------------------------------------------------------- */
  localparam NB_SIGNAL        = 2 ;                                       // Since input signal bits are going to be mapped to +/- 1,
                                                                          // we only need 2 bits to representate them using fxd point.
  localparam NB_MUL_FULL_RES  = NB_TAPS + NB_SIGNAL                     ;
  localparam NB_OUT           = NB_MUL_FULL_RES + (N_TAPS/os_rate - 1)  ; // NB considering all adders
  localparam N_FILTER_STAGES  = N_TAPS/os_rate                          ; // Number of regs and muls (polyphasic decomposition)
  localparam N_TAPS_PER_STAGE = N_TAPS/N_FILTER_STAGES                  ;
  localparam COUNTER_SIZE     = $clog2(os_rate)                         ;

/* VARIABLES -------------------------------------------------------------- */
  `include "filter_lut.v" // Filter taps file

  reg                                   shift_enable                          ;
  reg         [N_FILTER_STAGES - 1 : 0] shift_reg                             ;
  reg         [COUNTER_SIZE    - 1 : 0] counter                               ;
  reg signed  [NB_SIGNAL       - 1 : 0] mapped_data [N_FILTER_STAGES - 1 : 0] ;
  reg signed  [NB_MUL_FULL_RES - 1 : 0] mult        [N_FILTER_STAGES - 1 : 0] ;
  reg signed  [NB_OUT          - 1 : 0] sum                                   ;

/* Shift register --------------------------------------------------------- */
  always @(posedge i_clock or posedge i_reset) begin
    if (i_reset) begin
      shift_reg <= {N_FILTER_STAGES{1'b0}}  ;
    end
    else if (shift_enable) begin
      shift_reg <= {shift_reg[N_FILTER_STAGES-2:0], i_signal_bit_in}  ;
    end
  end

/* Counter to switch between taps and enable shift ------------------------ */
  always @(posedge i_clock or posedge i_reset) begin
    if (i_reset) begin
      counter <= {COUNTER_SIZE{1'b0}} ;
    end
    else if (i_enable) begin
      counter <= counter + 1  ;
    end
  end

/* Shift enabling every os_rate posedge clocks ---------------------------- */
  always @(*) begin
    if (counter == {COUNTER_SIZE{1'b1}}) begin
      shift_enable = 1'b1 ;
    end
    else begin
      shift_enable = 1'b0 ;
    end
  end

  integer i ; // for sentences purposes

/* Mapper ----------------------------------------------------------------- */
  always @(*) begin
    for (i = 0; i < N_FILTER_STAGES; i = i + 1) begin
      if (i_mapper_en == 1'b1) begin
        if (shift_reg[i] == 1'b0) begin
          mapped_data[i] = -1 ;
        end
        else begin
          mapped_data[i] = 1  ;
        end
      end
      else begin
        mapped_data[i] = shift_reg[i];
      end
    end
  end

/* Multipliers ------------------------------------------------------------ */
  always @(*) begin
    for (i = 0; i < N_FILTER_STAGES; i = i + 1) begin
      mult[i] = mapped_data[i] * coeffs_lut[i*N_TAPS_PER_STAGE + counter] ;
    end
  end

/* Adders ----------------------------------------------------------------- */
  always @(*) begin
    for (i = 0; i < N_FILTER_STAGES; i = i + 1) begin
      if (i == 0) begin
        sum = mult[i] ;
      end
      else begin
        sum = sum + mult[i] ;
      end
    end
  end

/* Output wiring ---------------------------------------------------------- */
  assign o_conv = i_enable == 1'b1 ? sum : 0;

endmodule