`timescale 1ns/100ps

module tb_reg_file ();
  
  localparam NB_CTRL_FRAME  = 32  ;
  localparam NB_FRAME_CMD   = 8   ;
  localparam NB_FRAME_EN    = 1   ;
  localparam NB_FRAME_DATA  = 23  ;

  localparam NB_I_MEM_DATA  = 64  ;
  localparam NB_O_DATA_BUS  = 32  ;
  localparam IQBITS         = 13  ;
  localparam NB_MEM_ADDR    = 12  ;

  reg                         i_clock             ;
  reg                         i_reset             ;
  reg   [ NB_CTRL_FRAME-1:0 ] i_frame             ;
  reg   [ IQBITS-1:0        ] i_data_log_from_mem ;
  reg                         i_memfull           ;
  reg   [ NB_I_MEM_DATA-1:0 ] i_ber_samp_I        ;
  reg   [ NB_I_MEM_DATA-1:0 ] i_ber_samp_Q        ;
  reg   [ NB_I_MEM_DATA-1:0 ] i_ber_error_I       ;
  reg   [ NB_I_MEM_DATA-1:0 ] i_ber_error_Q       ;
  wire  [ NB_O_DATA_BUS-1:0 ] o_data              ;
  wire                        o_rst               ;
  wire                        o_tx_en             ;
  wire                        o_rx_en             ;
  wire  [ 2:0               ] o_phase_sel         ;
  wire                        o_run_log           ;
  wire                        o_read_log          ;
  wire  [ NB_MEM_ADDR-1:0   ] o_addr_log_to_mem   ;

  wire posedge_det ;
  assign posedge_det = u_reg_file.reg_w_en;

  reg_file
    #(
      .NB_CTRL_FRAME  ( NB_CTRL_FRAME ) ,
      .NB_FRAME_CMD   ( NB_FRAME_CMD  ) ,
      .NB_FRAME_EN    ( NB_FRAME_EN   ) ,
      .NB_FRAME_DATA  ( NB_FRAME_DATA )
    )
    u_reg_file
    (
    .i_clock             ( i_clock              ) ,
    .i_reset             ( i_reset              ) ,
    .i_frame             ( i_frame              ) ,
    .i_data_log_from_mem ( i_data_log_from_mem  ) ,
    .i_memfull           ( i_memfull            ) ,
    .i_ber_samp_I        ( i_ber_samp_I         ) ,
    .i_ber_samp_Q        ( i_ber_samp_Q         ) ,
    .i_ber_error_I       ( i_ber_error_I        ) ,
    .i_ber_error_Q       ( i_ber_error_Q        ) ,
    .o_data              ( o_data               ) ,
    .o_rst               ( o_rst                ) ,
    .o_tx_en             ( o_tx_en              ) ,
    .o_rx_en             ( o_rx_en              ) ,
    .o_phase_sel         ( o_phase_sel          ) ,
    .o_run_log           ( o_run_log            ) ,
    .o_read_log          ( o_read_log           ) ,
    .o_addr_log_to_mem   ( o_addr_log_to_mem    )
    ) ;

  initial begin
    i_clock             = 1'b0            ;
    i_frame             = 1'b0            ;
    i_data_log_from_mem = 1'b0            ;
    i_memfull           = 1'b0            ;
    i_ber_samp_I        = 1'b0            ;
    i_ber_samp_Q        = 1'b0            ;
    i_ber_error_I       = 1'b0            ;
    i_ber_error_Q       = 1'b0            ;
    i_reset             = 1'b1            ;
    #1 i_reset          = 1'b0            ;
    
    /* TX ENABLE */
    #10 i_frame          = 32'h11000000   ;
    #10 i_frame          = 32'h11800000   ;
    #10 i_frame          = 32'h11000000   ;
    
    /* TX DISABLE */
    #10 i_frame          = 32'h10000000   ;
    #10 i_frame          = 32'h10800000   ;
    #10 i_frame          = 32'h10000000   ;

    /* RX ENABLE */
    #10 i_frame          = 32'h21000000   ;
    #10 i_frame          = 32'h21800000   ;
    #10 i_frame          = 32'h21000000   ;
    
    /* RX DISABLE */
    #10 i_frame          = 32'h20000000   ;
    #10 i_frame          = 32'h20800000   ;
    #10 i_frame          = 32'h20000000   ;

    /* RX PHASE 1 */
    #10 i_frame          = 32'h22000001   ;
    #10 i_frame          = 32'h22800001   ;
    #10 i_frame          = 32'h22000001   ;
    
    /* RX PHASE 3 */
    #10 i_frame          = 32'h22000003   ;
    #10 i_frame          = 32'h22800003   ;
    #10 i_frame          = 32'h22000003   ;

    /* LOG RUN */
    #10 i_frame          = 32'h33000000   ;
    #10 i_frame          = 32'h33800000   ;
    #10 i_frame          = 32'h33000000   ;
    
    /* LOG READ */
    #10 i_frame          = 32'h34000000   ;
    #10 i_frame          = 32'h34800000   ;
    #10 i_frame          = 32'h34000000   ;

    /* LOG ADDR 0x0FF */
    #10 i_frame          = 32'h350000FF   ;
    #10 i_frame          = 32'h358000FF   ;
    #10 i_frame          = 32'h350000FF   ;

    /* LOG ADDR 0xFAD */
    #10 i_frame          = 32'h35000FAD   ;
    #10 i_frame          = 32'h35800FAD   ;
    #10 i_frame          = 32'h35000FAD   ;

    /* RESET ENABLE */
    #10 i_frame          = 32'hF1000000   ;
    #10 i_frame          = 32'hF1800000   ;
    #10 i_frame          = 32'hF1000000   ;

    /* RESET DISABLE */
    #10 i_frame          = 32'hF0000000   ;
    #10 i_frame          = 32'hF0800000   ;
    #10 i_frame          = 32'hF0000000   ;
    
    #1000 $finish                         ;
  end

  always #5 i_clock = ~i_clock ;

endmodule