# Basic Communications System (QPSK transceiver) - Verilog Description

En este proyecto se realizó una integración del transceptor digital básico presentado en [un proyecto anterior](../Basic_Com_System_Implementation/), con un microcontrolador MicroBlaze (Xilinx IP), que se encarga de realizar el control del sistema. 

![](imgs/bloques2.svg)

![](imgs/bloques1.svg)

## MicroBlaze

El [firmware del microcontrolador](uC_Firmware/protocolo_main.c) escribe directamente el frame recibido por UART en sus puertos de salida (GPOs), repitiendo el proceso tres veces y negando el bit de habilitación de la trama. Este bit es utilizado por el register file para detectar la llegada de un nuevo comando. Además, cuando el comando recibido por UART lo requiere, lee el estado de las entradas de propósito general (GPIs) y lo envía a través de UART.

## Register File (RF)

Al [desarrollo anterior](../Basic_Com_System_Implementation/), se le agregó la descripción de un módulo denominado [register file](HDL/dsgn_src/reg_file.v). La instancia de este módulo se encuentra directamente conectada a las entradas/salidas de propósito general del microcontrolador como puede observarse en el [top](HDL/dsgn_src/top.v). Este módulo posee un detector de flancos positivos en el bit de habilitación de la trama, a fin de detectar la llegada de un nuevo comando. Cuando un nuevo comando es recibido, el módulo se encarga de actualizar las señales de control del transceptor de acuerdo a lo solicitado. Además, se encarga de direccionar los registros relativos a la información de Bit-Error Rate del sistema o del log de memoria RAM, de acuerdo con lo requerido por el usuario.

![](imgs/reg_file.svg)

## Memoria RAM

Otro agregado al sistema fue un [bloque de memoria RAM](HDL/dsgn_src/log_mem.v) (o Block RAM). Para ello se utilizó un template de Xilinx Vivado, para asegurarse de inferir Block RAM y no memoria RAM distribuida. El fin de este bloque de memoria es almacenar las señales de salida del transmisor. Junto con esta descripción, se utilizó un [bloque de control de memoria](HDL/dsgn_src/mem_control.v), provisto por Fundación Fulgor, cuya función es proporcionar las señales de control a la instancia de Block RAM (habilitación, dirección de memoria en la cual escribir/leer, etc). Además, este módulo contiene una bandera que indica cuando la memoria está llena.

![](imgs/RAM.svg)

## Interfaz de usuario

La interfaz de usuario consiste en un [script](Scripts/TxRx_UART_FPGA.py) de Python3. En este se generan y se envían, a través de puerto serie, las tramas de datos mencionadas. Las mismas consisten en:
- 8 bits de comando
- 1 bit de habilitación
- 23 bits de datos

La interfaz de usuario presenta un menú principal como el que se presenta a continuación. En él, se detallan los comandos a utilizar para controlar el sistema.

![](imgs/script_home.png)

Además, se incluyó un menú de ayuda que explica el uso de cada comando en particular y su funcionalidad, como se puede observar en la siguiente captura de pantalla.

![](imgs/script_help.png)

Por otra parte, el script recibe por UART información proveniente del microcontrolador. El usuario puede solicitar la lectura de la memoria RAM del sistema (que contiene parte de la señal transmitida), o el cálculo de Bit-Error Rate. Los datos recibidos de memoria RAM se encuentran en representación de punto fijo, y son convertidos a su representación decimal dentro del mismo script. Una vez convertidos, se almacenan en [archivos](Scripts/logs/) para su posterior uso.

En la figura siguiente, se presentan los gráficos de la señal transmitida, que fue almacenada en dichos [archivos](Scripts/logs/).
![](imgs/Tx_out.svg).

Debido a que se trabajó remotamente con las FPGA, se utilizó el bloque Virtual Input-Output (Xilinx IP), para monitorear el estado de las señales/indicadores del transceptor. El uso del mismo se presenta a continuación.

![](imgs/vio_ber_null.png)

En el caso presentado, se puede notar que el indicador de Bit-Error Rate indica que este valor es nulo. Esto se puede comprobar a través de los comandos de consola, solicitando al sistema información de BER, como se muestra a continuación.

![](imgs/script_ber_null.png)

Existe una fase de muestreo, para la cual no se pueden recuperar los símbolos transmitidos. En este caso, se trata de la fase 2. Como puede verse a continuación, tanto la interfaz de usuario por consola, como el VIO indican un valor de BER no nulo.

![](imgs/vio_ber_phase2.png)
![](imgs/script_ber_phase2.png)

## Esquemático completo del sistema
El esquemático final del sistema implementado se presenta a continuación.
![](imgs/top_completo.svg)

## Reportes de implementación de Vivado

La herramienta utilizada durante el diseño, Xilinx Vivado, presenta reportes post-implementación, que sirven al diseñador para evaluar distintos aspectos, y chequear el diseño.

En primer lugar, la herramienta presenta el porcentaje de utilización de los recursos del chip. Los porcentajes de utilización de recursos para la implementación presentada se muestran en el gráfico, y más detalladamente en la tabla, presentados a continuación. A partir de esto se puede concluir que, en promedio, se están utilizando un 20\% de los recursos de la FPGA utilizada (Artix-7 35T).

![](imgs/utilization_graph.png)
![](imgs/utilization_table.png)
        
En la Figura siguiente se presenta el layout del chip, donde los bloques de color celeste representan los recursos utilizados del chip.

![](imgs/layout.png)
        
Finalmente, se presenta el timing report de la implementación. 

![](imgs/timing_report.png)