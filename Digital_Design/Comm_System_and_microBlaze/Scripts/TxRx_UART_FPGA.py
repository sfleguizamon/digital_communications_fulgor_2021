'''
.SYNOPSIS
This program allows to communicate and control our MicroBlaze microcontroller through UART. 
.DESCRIPTION
Transmitted frame is 4 bytes in size, and it's composed as it follows:
| command | enable | data |
Command: 8-bits long.
Enable: 1-bit (must always be 0).
Data: 23-bits long.

.PARAMETER
It receives as argument the ttyUSB number connected to the FPGA.
.INPUTS
None
.OUTPUTS
None
.NOTES
   Version:        0.1
   Author:         Santiago F. Leguizamon
   Email:          stgoleguizamon@gmail.com
   Creation Date:  Friday, September 10th 2021, 10:17:05 am
   File: TxRx_UART_FPGA.py
HISTORY:
Date      	          By	Comments
----------	          ---	----------------------------------------------------------

.LINK
   https://gitlab.com/sfleguizamon

.COMPONENT
 Required Modules: 
    time, serial and sys.
'''

import time
import serial
import sys

from serial.serialutil import to_bytes

logs_path = "./logs/"

def frame_creator(option, suboption, data):
    frame_int_val = int(option, base = 16) + int(suboption, base = 16) + int(data, base = 16)
    frame = frame_int_val.to_bytes(4, byteorder = 'big')
    return frame

def fixedPointToFloat(number:int, NB:int, NBF:int):
        bin_str = bin(number)[2:]
        if len(bin_str) != NB:
            bin_str = bin_str.zfill(NB)
        binary = list(bin_str)
        for i in range(len(binary)):
            binary[i] = int(binary[i])
        value = 0
        if binary[0] == 1:
            for index in range(NB):
                if index == (0):
                    value = value - binary[index]*2**(NB-index-1)
                else:
                    value = value + binary[index]*2**(NB-index-1)
        else:
            for index in range(NB):
                value = value + binary[index]*2**(NB-index-1)
        return 1/(2**NBF) * value

def main():

    if len(sys.argv) > 2:
        print("WARNING: too many arguments entered.")
        return 1
    elif len(sys.argv) < 2:
        print("WARNING: not enough arguments entered. Using default serial port (ttyUSB5).")
        port = '/dev/ttyUSB5'
    else:
        port = '/dev/ttyUSB' + str(sys.argv[1])

    print("Using serial port {}." .format(port))

    ser = serial.Serial(
        port        = port                  ,	# Serial port configuration
        baudrate    = 115200                ,
        parity      = serial.PARITY_NONE    ,
        stopbits    = serial.STOPBITS_ONE   ,
        bytesize    = serial.EIGHTBITS
    )

    ser.isOpen()
    ser.timeout = 5

    main_menu = """
    \rTransceiver management:
    \r***********************
    \r  tx [enable/disable]
    \r  rx [enable/disable]
    \r  rx phase [0/1/2/3]
    \r  log [run/stop/read]
    \r  log addr [hexadecimal address]
    \r  ber read
    \r  reset [enable/disable]
    \r  help
    \r  quit
    \r<< """

    help_text = """
    \rTransceiver management (v0.1)
    \rThis script allows you to configurate your transceiver using the following commands:
    \r
    \rtx: transmitter management.
    \r    Usage: tx [arguments]
    \r    Arguments:
    \r        enable               Enable transmitter
    \r        disable              Disable transmitter
    \r
    \rrx: receiver management.
    \r    Usage: rx [arguments]
    \r    Arguments:
    \r        enable               Enable receiver
    \r        disable              Disable receiver
    \r        phase [data]         Selects sampling phase. Data must be a number between 0 and 3
    \r
    \rlog: transmitter output log management.
    \r    Usage: log [arguments]
    \r    Arguments:
    \r        run                  Start logging
    \r        stop                 Reset log enable signal
    \r        read [num]           Reads a certain number (num) of memory log entries 
    \r        addr                 Set a specific memory address to start reading 
    \r
    \rber: get bit-error rate for each channel.
    \r    Usage: ber [arguments]
    \r        read                 Read bit-error rate report.
    \r
    \rreset: system reset.
    \r    Usage: reset [arguments]
    \r    Arguments:
    \r        enable               Enable reset signal (set to 1)
    \r        disable              Disable reset signal (set to 0)
    \r
    \rhelp: print help (this message) and exit.
    \r    Usage: help
    \r
    \rquit: exit.
    \r    Usage: quit
    """

    optionsDict = {
        'TX'    : '0x10000000',
        'RX'    : '0x20000000',
        'LOG'   : '0x30000000',
        'BER'   : '0x40000000',
        'RESET' : '0xF0000000',
        'HELP'  : 'help',
        'QUIT'  : 'quit'
    }
    suboptionsDict = {              # Bit 24 is always RESERVED and must be 0.
        'DISABLE'   : '0x0000000',
        'ENABLE'    : '0x1000000',
        'PHASE'     : '0x2000000',
        'RUN'       : '0x3000000',
        'STOP'      : '0x4000000',
        'READ'      : '0x5000000', 
        'ADDR'      : '0x6000000',    # bits 12:0 reserved for address
        'COUNT_I'   : '0x7000000',
        'COUNT_Q'   : '0x8000000',
        'ERR_I'     : '0x9000000',
        'ERR_Q'     : '0xA000000'
    }

    while True:

        cfg_opt = input(main_menu).upper().split()

        if cfg_opt[0] in optionsDict: # Checking for correct option

            if cfg_opt[0] == 'QUIT':
                ser.close()
                return 0

            elif cfg_opt[0] == 'HELP':
                print(help_text)
                ser.close()
                return 0

            else:            
                if cfg_opt[1] in suboptionsDict: # Checking for correct suboption

                    # tx and reset options
                    if cfg_opt[0] == 'TX' or cfg_opt[0] == 'RESET':

                        if cfg_opt[1] == 'ENABLE' or cfg_opt[1] == 'DISABLE':
                            data = '0x0'

                    # rx options
                    elif cfg_opt[0] == 'RX':

                        if cfg_opt[1] == 'ENABLE' or cfg_opt[1] == 'DISABLE':
                            data = '0x0'

                        elif cfg_opt[1] == 'PHASE':

                            if len(cfg_opt) == 3:
                                data = cfg_opt[2]

                            else:
                                data = '0x0'
                                print('WARNING: no phase value was entered. Setting phase = 0.')

                    # memory log options
                    elif cfg_opt[0] == 'LOG':

                        if cfg_opt[1] == 'RUN' or cfg_opt[1] == 'STOP':
                            data = '0x0'

                        elif cfg_opt[1] == 'READ':
                            if len(cfg_opt) == 3:
                                interations = int(cfg_opt[2])
                            else:
                                interations = 10
                            log_file_i = open(logs_path + "mem_log_i.txt", "w")
                            log_file_q = open(logs_path + "mem_log_q.txt", "w")

                            for i in range(interations):

                                # Increasing address value by addr command
                                data = hex(i)
                                frame = frame_creator(optionsDict['LOG'], suboptionsDict['ADDR'], data)
                                ser.write(frame)

                                # Sending reading command
                                data = '0x0'
                                frame = frame_creator(optionsDict[cfg_opt[0]], suboptionsDict[cfg_opt[1]], data)
                                ser.write(frame)

                                # Reading data
                                readData = ser.read(4)
                                # Bytes to frame int value
                                int_value = int.from_bytes(readData,byteorder='big')

                                # Frame int value to float (fixed point to float) 
                                data_i = fixedPointToFloat(int_value>>15, 15, 7)
                                data_q = fixedPointToFloat(int_value&int("0x7FFF", base = 16), 15, 7)

                                # Saving value in to file
                                log_file_i.write("{}\n".format(data_i))
                                log_file_q.write("{}\n".format(data_q))

                            log_file_i.close()
                            log_file_q.close()

                        elif cfg_opt[1] == 'ADDR':
                            if len(cfg_opt) == 3:
                                data = cfg_opt[2]
                            else:
                                data = '0x0'
                                print('WARNING: no address value was entered. Reading memory addr 0x0000.')
                    
                    elif cfg_opt[0] == 'BER':
                        
                        # Getting number of samples
                        for i in range(0,2):
                            data = hex(i) # i selects between high (0) and low (1) parts of input data
                            frame = frame_creator(optionsDict['BER'], suboptionsDict['COUNT_I'], data)
                            ser.write(frame)
                            readData = ser.read(4)
                            out = int.from_bytes(readData,byteorder='big')
                            if i == 0:
                                total_i = out << 32
                            else:
                                total_i = total_i | out

                        # Getting number of errors
                        for i in range(0,2):
                            data = hex(i) # i selects between high (0) and low (1) parts of input data
                            frame = frame_creator(optionsDict['BER'], suboptionsDict['ERR_I'], data)
                            ser.write(frame)
                            readData = ser.read(4)
                            out = int.from_bytes(readData,byteorder='big')
                            if i == 0:
                                errors_i = out << 32
                            else:
                                errors_i = errors_i | out
                        
                        # Getting number of samples
                        for i in range(0,2):
                            data = hex(i) # i selects between high (0) and low (1) parts of input data
                            frame = frame_creator(optionsDict['BER'], suboptionsDict['COUNT_Q'], data)
                            ser.write(frame)
                            readData = ser.read(4)
                            out = int.from_bytes(readData,byteorder='big')
                            if i == 0:
                                total_q = out << 32
                            else:
                                total_q = total_q | out

                        # Getting number of errors
                        for i in range(0,2):
                            data = hex(i) # i selects between high (0) and low (1) parts of input data
                            frame = frame_creator(optionsDict['BER'], suboptionsDict['ERR_Q'], data)
                            ser.write(frame)
                            readData = ser.read(4)
                            out = int.from_bytes(readData,byteorder='big')
                            if i == 0:
                                errors_q = out << 32
                            else:
                                errors_q = errors_q | out
                        
                        print("BER I channel = {}".format(errors_i/total_i))
                        print("BER Q channel = {}".format(errors_q/total_q))

                    if cfg_opt[1] != 'READ' and cfg_opt[0] != 'BER':
                        frame = frame_creator(optionsDict[cfg_opt[0]], suboptionsDict[cfg_opt[1]], data)
                        ser.write(frame)

                else:
                    print('ERROR: invalid suboption.')
                    ser.close()
                    return 1       
        else:
            print("ERROR: invalid option.")
            ser.close()
            return 1

if __name__ == "__main__":
    main()