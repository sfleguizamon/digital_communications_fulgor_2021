import numpy as np
import matplotlib.pyplot as plt

plots_path = "imgs/"
logs_path = "Scripts/logs/"
data_I = np.loadtxt(logs_path + "mem_log_i.txt")
data_Q = np.loadtxt(logs_path + "mem_log_q.txt")

plt.figure(figsize=[10, 6.5])
plt.subplot(2,1,1)
plt.plot(data_I, '-r', linewidth = 1.5, label = "Tx output (In-Phase component)")
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(0,100); plt.ylim(-1.5, 1.5); plt.title('Tx output (from memory)')
plt.subplot(2,1,2)
plt.plot(data_Q, '-r', linewidth = 1.5, label = "Tx output (Quadrature component)")
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(0, 100); plt.ylim(-1.5, 1.5)
plt.savefig(plots_path + 'Tx_out.svg', format = 'svg')
plt.show()