'''
.SYNOPSIS
This script pretends to show the impact of fixed point arithmetic implementation 
over a raised cosine filter coefficients, used in a QPSK elemental transceiver. 
This effect is also contrasted to floating point arithmetic implementation.
Plots shows the mentioned effects on 3 different cases, each corresponding to a different
pulse-shaping filter rolloff.
.DESCRIPTION
<Brief description of script>
.PARAMETER <Parameter_Name>
<Brief description of parameter input required. Repeat this attribute if required>
.INPUTS
<Inputs if any, otherwise state None>
.OUTPUTS
<Outputs if anything is generated>
.NOTES
   Version:        0.1
   Author:         Santiago F. Leguizamon
   Email:          stgoleguizamon@gmail.com
   Creation Date:  Tuesday, June 29th 2021, 8:40:37 pm
   File: fxd_quatization_analysis.py
   Copyright (c) 2021 Santiago F. Leguizamon
HISTORY:
Date      	          By	Comments
----------	          ---	----------------------------------------------------------

.LINK
   https://github.com/sfleguizamon

.COMPONENT
 Required Modules: numpy, matplotlib, scipy, DSPtools and _fixedInt.
    DSPtools and _fixedInt modules were provided by Fundacion Fulgor.
'''

import os
from matplotlib.colors import Colormap
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq, fftshift
from tool._fixedInt import *
from tool.DSPtools import *

plots_path     = 'plots/'
reports_path   = 'reports/'

# GENERAL PARAMETERS
BR              = 1e9               # Symbol rate
T               = 1/BR              # Time between symbols
oversampling    = 8                 # Oversampling rate
sim_lenght      = 1000              # Simulation length (symbols quantity)
Ts              = T/oversampling    # Sampling period (1/fs)
NFFT            = 1024*8            # FFT points

# FILTER PARAMETERS
rolloff         = [0.0, 0.5, 1.0]   # Raised cosine filter rolloff
Nzc             = 16                # Number of zero crossings
Norm            = False             # Filter amplitude normalization enable
                                    # (normalization on freq domain)

# QUANTIZATION PARAMETERS (Fixed point)

NB  = 3
NBF = 2

# ---------------------------------------------------------------------------------------------------------------------------
# ----------------------------------------------- Impulse responses generation ----------------------------------------------

(t1, h1) = rcosine(rolloff[0], T, oversampling, Nzc, Norm)
(t2, h2) = rcosine(rolloff[1], T, oversampling, Nzc, Norm)
(t3, h3) = rcosine(rolloff[2], T, oversampling, Nzc, Norm)

# Quantization S(NB, NBF)

h1_fxd_trunc = fxd_quantization(h1, NB, NBF, True, False, True)
h1_fxd_round = fxd_quantization(h1, NB, NBF, True, True, True)

h2_fxd_trunc = fxd_quantization(h2, NB, NBF, True, False, True)
h2_fxd_round = fxd_quantization(h2, NB, NBF, True, True, True)

h3_fxd_trunc = fxd_quantization(h3, NB, NBF, True, False, True)
h3_fxd_round = fxd_quantization(h3, NB, NBF, True, True, True)

# Impulse response plots

plt.figure(figsize = [15, 5]) # Impulse responses plots

plt.subplot(1, 3, 1)
plt.plot(t1, h1, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[0]))
plt.plot(t1, h1_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[0], NB, NBF))
plt.plot(t1, h1_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[0], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(-Nzc/2/BR,Nzc/2/BR); plt.title('Impulse responses')

plt.subplot(1, 3, 2)
plt.plot(t2, h2, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[1]))
plt.plot(t2, h2_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[1], NB, NBF))
plt.plot(t2, h2_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[1], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(-Nzc/2/BR,Nzc/2/BR); plt.title('Impulse responses')

plt.subplot(1, 3, 3)
plt.plot(t3, h3, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[2]))
plt.plot(t3, h3_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[2], NB, NBF))
plt.plot(t3, h3_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[2], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(-Nzc/2/BR,Nzc/2/BR); plt.title('Impulse responses')

plt.savefig(plots_path + 'Impulse_responses_fxd_S({}-{}).svg'.format(NB, NBF), format = 'svg')
# ---------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------- Frequency responses generation and plots -----------------------------------------

mod1            = fftshift( np.log10( abs( fft(h1, NFFT) ) ) )
mod1_fxd_trunc  = fftshift( np.log10( abs( fft(h1_fxd_trunc, NFFT) ) ) )
mod1_fxd_round  = fftshift( np.log10( abs( fft(h1_fxd_round, NFFT) ) ) )

mod2            = fftshift( np.log10( abs( fft(h2, NFFT) ) ) )
mod2_fxd_trunc  = fftshift( np.log10( abs( fft(h2_fxd_trunc, NFFT) ) ) )
mod2_fxd_round  = fftshift( np.log10( abs( fft(h2_fxd_round, NFFT) ) ) )

mod3            = fftshift( np.log10( abs( fft(h3, NFFT) ) ) )
mod3_fxd_trunc  = fftshift( np.log10( abs( fft(h3_fxd_trunc, NFFT) ) ) )
mod3_fxd_round  = fftshift( np.log10( abs( fft(h3_fxd_round, NFFT) ) ) )

freq            = fftshift( fftfreq(NFFT, Ts) ) / 1e9   # Convertido a GHz

plt.figure(figsize = [15, 5]) # Freq responses plots

plt.subplot(1, 3, 1)
plt.plot(freq, mod1, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[0]))
plt.plot(freq, mod1_fxd_trunc, '-g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[0], NB, NBF))
plt.plot(freq, mod1_fxd_round, '-k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[0], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Frequency [GHz]'); plt.ylabel('Magnitude [dB]')
plt.xlim(-2, 2); plt.ylim(-5, 1); plt.title('Frequency responses')

plt.subplot(1, 3, 2)
plt.plot(freq, mod2, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[1]))
plt.plot(freq, mod2_fxd_trunc, '-g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[1], NB, NBF))
plt.plot(freq, mod2_fxd_round, '-k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[1], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Frequency [GHz]'); plt.ylabel('Magnitude [dB]')
plt.xlim(-2, 2); plt.ylim(-5, 1); plt.title('Frequency responses')

plt.subplot(1, 3, 3)
plt.plot(freq, mod3, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[2]))
plt.plot(freq, mod3_fxd_trunc, '-g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[2], NB, NBF))
plt.plot(freq, mod3_fxd_round, '-k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[2], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Frequency [GHz]'); plt.ylabel('Magnitude [dB]')
plt.xlim(-2, 2); plt.ylim(-5, 1); plt.title('Frequency responses')

plt.savefig(plots_path + 'Frequency_responses_fxd_S({}-{}).svg'.format(NB, NBF), format = 'svg')

# ---------------------------------------------------------------------------------------------------------------------------
# ---------------------------------------- Random symbols generation and upsampling -----------------------------------------

symbolsI = 2*(np.random.uniform(-1,1,sim_lenght)>0.0)-1
symbolsQ = 2*(np.random.uniform(-1,1,sim_lenght)>0.0)-1
symbolsI = upsample(symbolsI, oversampling)
symbolsQ = upsample(symbolsQ, oversampling)

# ---------------------------------------------------------------------------------------------------------------------------
# ------------------------ Filtering (interpolation and pulse-shaping) and interpolated signal plots ------------------------

tx_I_h1 = np.convolve(symbolsI, h1, 'valid')
tx_Q_h1 = np.convolve(symbolsQ, h1, 'valid')

tx_I_h1_fxd_trunc = np.convolve(symbolsI, h1_fxd_trunc, 'valid')
tx_Q_h1_fxd_trunc = np.convolve(symbolsQ, h1_fxd_trunc, 'valid')

tx_I_h1_fxd_round = np.convolve(symbolsI, h1_fxd_round, 'valid')
tx_Q_h1_fxd_round = np.convolve(symbolsQ, h1_fxd_round, 'valid')

tx_I_h2 = np.convolve(symbolsI, h2, 'valid')
tx_Q_h2 = np.convolve(symbolsQ, h2, 'valid')

tx_I_h2_fxd_trunc = np.convolve(symbolsI, h2_fxd_trunc, 'valid')
tx_Q_h2_fxd_trunc = np.convolve(symbolsQ, h2_fxd_trunc, 'valid')

tx_I_h2_fxd_round = np.convolve(symbolsI, h2_fxd_round, 'valid')
tx_Q_h2_fxd_round = np.convolve(symbolsQ, h2_fxd_round, 'valid')

tx_I_h3 = np.convolve(symbolsI, h3, 'valid')
tx_Q_h3 = np.convolve(symbolsQ, h3, 'valid')

tx_I_h3_fxd_trunc = np.convolve(symbolsI, h3_fxd_trunc, 'valid')
tx_Q_h3_fxd_trunc = np.convolve(symbolsQ, h3_fxd_trunc, 'valid')

tx_I_h3_fxd_round = np.convolve(symbolsI, h3_fxd_round, 'valid')
tx_Q_h3_fxd_round = np.convolve(symbolsQ, h3_fxd_round, 'valid')

plt.figure(figsize = [15, 5]) # Interpolated signals plots (In-Phase components)

plt.subplot(1, 3, 1)
plt.plot(tx_I_h1, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[0]))
plt.plot(tx_I_h1_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[0], NB, NBF))
plt.plot(tx_I_h1_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[0], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(200,500); plt.title('In-Phase signal component')

plt.subplot(1, 3, 2)
plt.plot(tx_I_h2, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[1]))
plt.plot(tx_I_h2_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[1], NB, NBF))
plt.plot(tx_I_h2_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[1], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(200,500); plt.title('In-Phase signal component')

plt.subplot(1, 3, 3)
plt.plot(tx_I_h3, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[2]))
plt.plot(tx_I_h3_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[2], NB, NBF))
plt.plot(tx_I_h3_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[2], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(200,500); plt.title('In-Phase signal component')

plt.savefig(plots_path + 'Interpolated_signals_I_fxd_S({}-{}).svg'.format(NB, NBF), format = 'svg')

plt.figure(figsize = [15, 5]) # Interpolated signals plots (Quadrature components)

plt.subplot(1, 3, 1)
plt.plot(tx_Q_h1, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[0]))
plt.plot(tx_Q_h1_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[0], NB, NBF))
plt.plot(tx_Q_h1_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[0], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(200,500); plt.title('Quadrature signal component')

plt.subplot(1, 3, 2)
plt.plot(tx_Q_h2, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[1]))
plt.plot(tx_Q_h2_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[1], NB, NBF))
plt.plot(tx_Q_h2_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[1], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(200,500); plt.title('Quadrature signal component')

plt.subplot(1, 3, 3)
plt.plot(tx_Q_h3, '-r', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff[2]))
plt.plot(tx_Q_h3_fxd_trunc, '-.g', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[2], NB, NBF))
plt.plot(tx_Q_h3_fxd_round, '--k', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[2], NB, NBF))
plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
plt.xlim(200,500); plt.title('Quadrature signal component')

plt.savefig(plots_path + 'Interpolated_signals_Q_fxd_S({}-{}).svg'.format(NB, NBF), format = 'svg')

# ---------------------------------------------------------------------------------------------------------------------------
# --------------------------------------- Downsampling and constellation diagrams -------------------------------------------

phase = 1

rx_I_h1 = downsample(tx_I_h1, oversampling, phase)
rx_Q_h1 = downsample(tx_Q_h1, oversampling, phase)

rx_I_h1_fxd_trunc = downsample(tx_I_h1_fxd_trunc, oversampling, phase)
rx_Q_h1_fxd_trunc = downsample(tx_Q_h1_fxd_trunc, oversampling, phase)

rx_I_h1_fxd_round = downsample(tx_I_h1_fxd_round, oversampling, phase)
rx_Q_h1_fxd_round = downsample(tx_Q_h1_fxd_round, oversampling, phase)

rx_I_h2 = downsample(tx_I_h2, oversampling, phase)
rx_Q_h2 = downsample(tx_Q_h2, oversampling, phase)

rx_I_h2_fxd_trunc = downsample(tx_I_h2_fxd_trunc, oversampling, phase)
rx_Q_h2_fxd_trunc = downsample(tx_Q_h2_fxd_trunc, oversampling, phase)

rx_I_h2_fxd_round = downsample(tx_I_h2_fxd_round, oversampling, phase)
rx_Q_h2_fxd_round = downsample(tx_Q_h2_fxd_round, oversampling, phase)

rx_I_h3 = downsample(tx_I_h3, oversampling, phase)
rx_Q_h3 = downsample(tx_Q_h3, oversampling, phase)

rx_I_h3_fxd_trunc = downsample(tx_I_h3_fxd_trunc, oversampling, phase)
rx_Q_h3_fxd_trunc = downsample(tx_Q_h3_fxd_trunc, oversampling, phase)

rx_I_h3_fxd_round = downsample(tx_I_h3_fxd_round, oversampling, phase)
rx_Q_h3_fxd_round = downsample(tx_Q_h3_fxd_round, oversampling, phase)

plt.figure(figsize = [15, 5]) # Constellation diagrams

plt.subplot(1, 3, 1)
plt.scatter(rx_I_h1, rx_Q_h1, c = 'r', marker='o',label = r'$\beta={},\ Float$'.format(rolloff[0]))
plt.scatter(rx_I_h1_fxd_trunc, rx_Q_h1_fxd_trunc, c = 'g', marker='v',label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[0], NB, NBF))
plt.scatter(rx_I_h1_fxd_round, rx_Q_h1_fxd_round, c = 'k', marker='*',label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[0], NB, NBF))
plt.grid(); plt.legend(loc = 'best', fontsize = 'x-small'); plt.xlabel('In-Phase'); plt.ylabel('Quadrature')
plt.title('Constellation diagram')

plt.subplot(1, 3, 2)
plt.scatter(rx_I_h2, rx_Q_h2, c = 'r', marker='o',label = r'$\beta={},\ Float$'.format(rolloff[1]))
plt.scatter(rx_I_h2_fxd_trunc, rx_Q_h2_fxd_trunc, c = 'g', marker='v',label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[1], NB, NBF))
plt.scatter(rx_I_h2_fxd_round, rx_Q_h2_fxd_round, c = 'k', marker='*',label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[1], NB, NBF))
plt.grid(); plt.legend(loc = 'best', fontsize = 'x-small'); plt.xlabel('In-Phase'); plt.ylabel('Quadrature')
plt.title('Constellation diagram')

plt.subplot(1, 3, 3)
plt.scatter(rx_I_h3, rx_Q_h3, c = 'r', marker='o',label = r'$\beta={},\ Float$'.format(rolloff[2]))
plt.scatter(rx_I_h3_fxd_trunc, rx_Q_h3_fxd_trunc, c = 'g', marker='v',label = r'$\beta={},\ S({},{}),\ Trunc$'.format(rolloff[2], NB, NBF))
plt.scatter(rx_I_h3_fxd_round, rx_Q_h3_fxd_round, c = 'k', marker='*',label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff[2], NB, NBF))
plt.grid(); plt.legend(loc = 'best', fontsize = 'x-small'); plt.xlabel('In-Phase'); plt.ylabel('Quadrature')
plt.title('Constellation diagram')

plt.savefig(plots_path + 'Constellation_diagrams_fxd_S({}-{}).svg'.format(NB, NBF), format = 'svg')

plt.show()

# ---------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------- SNR calculation -------------------------------------------------------

I_signal_power = np.sum(tx_I_h1**2)/len(tx_I_h1)
Q_signal_power = np.sum(tx_Q_h1**2)/len(tx_Q_h1)

I_error_power_trunc  = np.sum((tx_I_h1 - tx_I_h1_fxd_trunc)**2)/len(tx_I_h1)
Q_error_power_trunc  = np.sum((tx_Q_h1 - tx_Q_h1_fxd_trunc)**2)/len(tx_Q_h1)

I_error_power_round  = np.sum((tx_I_h1 - tx_I_h1_fxd_round)**2)/len(tx_I_h1)
Q_error_power_round  = np.sum((tx_Q_h1 - tx_Q_h1_fxd_round)**2)/len(tx_Q_h1)

I_SNR_trunc = 10*np.log10(I_signal_power/I_error_power_trunc)
Q_SNR_trunc = 10*np.log10(Q_signal_power/Q_error_power_trunc)

I_SNR_round = 10*np.log10(I_signal_power/I_error_power_round)
Q_SNR_round = 10*np.log10(Q_signal_power/Q_error_power_round)

# Report file creation

if(os.path.isfile(reports_path + 'SNR_report.csv')): # Checks if the report file already exists (header purposes)
    SNRreport = ''
else:
    SNRreport = 'Fxd quantization,Mode,SNR [dB] (I component),SNR [dB] (Q component)'

SNRreport = SNRreport + '\nS({}; {}),Truncated,{},{}'.format(NB, NBF, I_SNR_trunc, Q_SNR_trunc)
SNRreport = SNRreport + '\nS({}; {}),Rounded,{},{}'.format(NB, NBF, I_SNR_round, Q_SNR_round)

f = open(reports_path + 'SNR_report.csv', 'a')  # 'a' = append mode
f.write(SNRreport)
f.close()