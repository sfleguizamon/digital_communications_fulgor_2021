'''
.SYNOPSIS
This script consists of the basic structure of an elemental QPSK transceiver. It 
compares the effect of using different rolloffs for the pulse-shaping filter.
.DESCRIPTION
It consists of a raised cosine
.PARAMETER <Parameter_Name>
<Brief description of parameter input required. Repeat this attribute if required>
.INPUTS
<Inputs if any, otherwise state None>
.OUTPUTS
<Outputs if anything is generated>
.NOTES
   Version:        0.1
   Author:         Santiago F. Leguizamon
   Email:          stgoleguizamon@gmail.com
   Creation Date:  Tuesday, June 29th 2021, 5:33:10 pm
   File: QPSK_transceiver_elemental.py
   Copyright (c) 2021 Santiago F. Leguizamon
HISTORY:
Date      	          By	Comments
----------	          ---	----------------------------------------------------------

.LINK
   https://github.com/sfleguizamon

.COMPONENT
 Required Modules: numpy, matplotlib, scipy, DSPtools and _fixedInt.
    DSPtools and _fixedInt modules were provided by Fundacion Fulgor.
'''

import os
from matplotlib.colors import Colormap
import numpy as np
import matplotlib.pyplot as plt
from scipy.fft import fft, fftfreq, fftshift
from tool._fixedInt import *
from tool.DSPtools import *

plots_path     = 'plots/'
reports_path   = 'reports/'

# GENERAL PARAMETERS
BR              = 1e9               # Symbol rate
T               = 1/BR              # Time between symbols
oversampling    = 8                 # Oversampling rate
sim_lenght      = 1000              # Simulation length (symbols quantity)
Ts              = T/oversampling    # Sampling period (1/fs)
NFFT            = 1024*8            # FFT points

# FILTER PARAMETERS
rolloff         = [0.0, 0.5, 1.0]   # Raised cosine filter rolloff
Nzc             = 16                # Number of zero crossings
Norm            = False             # Filter amplitude normalization enable
                                    # (normalization on freq domain)


# Impulse responses generation and plots

(t1, h1) = rcosine(rolloff[0], T, oversampling, Nzc, Norm)
(t2, h2) = rcosine(rolloff[1], T, oversampling, Nzc, Norm)
(t3, h3) = rcosine(rolloff[2], T, oversampling, Nzc, Norm)

plt.figure(figsize = [5.5, 5.5])
plt.plot(t1, h1, '-r', linewidth = 1, label = r'$\beta={}$'.format(rolloff[0]))
plt.plot(t2, h2, '-g', linewidth = 1, label = r'$\beta={}$'.format(rolloff[1]))
plt.plot(t3, h3, '-k', linewidth = 1, label = r'$\beta={}$'.format(rolloff[2]))
plt.grid()
plt.legend()
plt.xlabel('Samples')
plt.ylabel('Amplitude')
plt.title('Impulse responses')
plt.xlim(-Nzc/2/BR,Nzc/2/BR)
plt.savefig(plots_path + 'Filter_impulse_responses.svg', format = 'svg')

# Frequency responses generation and plots

mod1    = fftshift( np.log10( abs( fft(h1, NFFT) ) ) )
freq1   = fftshift( fftfreq(NFFT, Ts) ) / 1e9   # Convertido a GHz

mod2    = fftshift( np.log10( abs( fft(h2, NFFT) ) ) )
freq2   = fftshift( fftfreq(NFFT, Ts) ) / 1e9   # Convertido a GHz

mod3    = fftshift( np.log10( abs( fft(h3, NFFT) ) ) )
freq3   = fftshift( fftfreq(NFFT, Ts) ) / 1e9   # Convertido a GHz

plt.figure(figsize = [5.5, 5.5])
plt.plot(freq1, mod1, '-r', linewidth = 1, label = r'$\beta={}$'.format(rolloff[0]))
plt.plot(freq2, mod2, '-g', linewidth = 1, label = r'$\beta={}$'.format(rolloff[1]))
plt.plot(freq3, mod3, '-k', linewidth = 1, label = r'$\beta={}$'.format(rolloff[2]))
plt.grid()
plt.legend()
plt.xlabel('Frequency [GHz]')
plt.ylabel('Magnitude [dB]')
plt.title('Frequency responses')
plt.xlim(-2, 2)
plt.ylim(-6, 1)
plt.savefig(plots_path + 'Filter_frequency_responses.svg', format = 'svg')

# Random symbols generation

symbolsI = 2*(np.random.uniform(-1,1,sim_lenght)>0.0)-1
symbolsQ = 2*(np.random.uniform(-1,1,sim_lenght)>0.0)-1

symbolsI = upsample(symbolsI, oversampling)
symbolsQ = upsample(symbolsQ, oversampling)

# Filtering (interpolation and pulse-shaping) and interpolated signal plots

tx_I_h1 = np.convolve(symbolsI, h1, 'valid')
tx_Q_h1 = np.convolve(symbolsQ, h1, 'valid')

tx_I_h2 = np.convolve(symbolsI, h2, 'valid')
tx_Q_h2 = np.convolve(symbolsQ, h2, 'valid')

tx_I_h3 = np.convolve(symbolsI, h3, 'valid')
tx_Q_h3 = np.convolve(symbolsQ, h3, 'valid')


plt.figure(figsize = [5.5, 5.5])
plt.plot(tx_I_h1,'-r', linewidth = 1, label = r'$\beta={}$'.format(rolloff[0]))
plt.plot(tx_I_h2,'-g', linewidth = 1, label = r'$\beta={}$'.format(rolloff[1]))
plt.plot(tx_I_h3,'-k', linewidth = 1, label = r'$\beta={}$'.format(rolloff[2]))
plt.xlabel('Samples')
plt.ylabel('Amplitude')
plt.title('In-Phase component')
plt.xlim(200,500)
plt.grid()
plt.legend()
plt.savefig(plots_path + 'Interpolated_signal_I.svg', format = 'svg')

plt.figure(figsize = [5.5, 5.5])
plt.plot(tx_Q_h1,'-r', linewidth = 1, label = r'$\beta={}$'.format(rolloff[0]))
plt.plot(tx_Q_h2,'-g', linewidth = 1, label = r'$\beta={}$'.format(rolloff[1]))
plt.plot(tx_Q_h3,'-k', linewidth = 1, label = r'$\beta={}$'.format(rolloff[2]))
plt.xlabel('Samples')
plt.ylabel('Amplitude')
plt.title('Quadrature component')
plt.xlim(200,500)
plt.grid()
plt.legend()
plt.savefig(plots_path + 'Interpolated_signal_Q.svg', format = 'svg')

# Downsampling and constellation diagram

phase = 1

rx_I_h1 = downsample(tx_I_h1, oversampling, phase)
rx_Q_h1 = downsample(tx_Q_h1, oversampling, phase)

rx_I_h2 = downsample(tx_I_h2, oversampling, phase)
rx_Q_h2 = downsample(tx_Q_h2, oversampling, phase)

rx_I_h3 = downsample(tx_I_h3, oversampling, phase)
rx_Q_h3 = downsample(tx_Q_h3, oversampling, phase)

plt.figure(figsize = [5.5, 5.5])
plt.scatter(rx_I_h1, rx_Q_h1, c = 'r', marker='o',label = r'$\beta={}$'.format(rolloff[0]))
plt.scatter(rx_I_h2, rx_Q_h2, c = 'g', marker='v',label = r'$\beta={}$'.format(rolloff[1]))
plt.scatter(rx_I_h3, rx_Q_h3, c = 'k', marker='*',label = r'$\beta={}$'.format(rolloff[2]))
plt.xlabel('In-Phase')
plt.ylabel('Quadrature')
plt.title('Constellation diagram')
plt.grid()
plt.legend()
plt.savefig(plots_path + 'Constellation_diagrams.svg', format = 'svg')

plt.show()