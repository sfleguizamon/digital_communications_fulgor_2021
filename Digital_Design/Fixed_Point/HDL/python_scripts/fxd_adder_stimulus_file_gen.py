'''
.SYNOPSIS
This script generates random numbers and quantizates them. The purpose is to generate
two files which will be used in the Verilog test bench of this project.
.DESCRIPTION
<Brief description of script>
.PARAMETER <Parameter_Name>
<Brief description of parameter input required. Repeat this attribute if required>
.INPUTS
<Inputs if any, otherwise state None>
.OUTPUTS
<Outputs if anything is generated>
.NOTES
   Version:        0.1
   Author:         Santiago F. Leguizamon
   Email:          stgoleguizamon@gmail.com
   Creation Date:  Tuesday, July 6th 2021, 4:05:02 pm
   File: stimulus_file_gen.py
   Copyright (c) 2021 Santiago F. Leguizamon
HISTORY:
Date      	          By	Comments
----------	          ---	----------------------------------------------------------

.LINK
   https://github.com/sfleguizamon

.COMPONENT
 Required Modules: 

'''

import numpy as np
from tool._fixedInt import *
from tool.DSPtools import fxd_quantization

output_path = 'stimul/'

# FIXED POINT PARAMETERS

# Inputs
NB_a    = 16
NBF_a   = 14

NB_b    = 12
NBF_b   = 11

in_signedMode  = 'S'
in_roundMode   = 'round'
in_satureMode  = 'saturate'

# Outputs
NB_OUT_FULL     = 17
NBF_OUT_FULL    = 14

NB_OUT_TRUNC    = 11
NBF_OUT_TRUNC   = 10

NB_OUT_ROUND    = 9
NBF_OUT_ROUND   = 8

a = DeFixedInt(NB_a, NBF_a, in_signedMode, in_roundMode, in_satureMode)
b = DeFixedInt(NB_b, NBF_b, in_signedMode, in_roundMode, in_satureMode)
c_full = DeFixedInt(NB_OUT_FULL, NBF_OUT_FULL, 'S', 'round', 'saturate')
c_ovf_trunc = DeFixedInt(NB_OUT_TRUNC, NBF_OUT_TRUNC, 'S', 'trunc', 'wrap')
c_sat_trunc = DeFixedInt(NB_OUT_TRUNC, NBF_OUT_TRUNC, 'S', 'trunc', 'saturate')
c_sat_round = DeFixedInt(NB_OUT_ROUND, NBF_OUT_ROUND, 'S', 'round', 'saturate')

Range_a = a.getRange()
Range_b = b.getRange()

sim_length = 1000

A_file = open(output_path + 'addend_A.stim', 'w')
B_file = open(output_path + 'addend_B.stim', 'w')
C_file = open(output_path + 'sum.out', 'w')

for i in range(sim_length):

    a.value = np.random.uniform(low = Range_a[0], high = Range_a[1])
    b.value = np.random.uniform(low = Range_b[0], high = Range_b[1])
    c_full.assign(a+b)
    c_ovf_trunc.assign(a+b)
    c_sat_trunc.assign(a+b)
    c_sat_round.assign(a+b)

    A_file.write(bin(a.intvalue)[2:].zfill(a.width))
    B_file.write(bin(b.intvalue)[2:].zfill(b.width))
    C_file.write(bin(c_full.intvalue)[2:].zfill(c_full.width)   + '\t')
    C_file.write(bin(c_ovf_trunc.intvalue)[2:].zfill(c_ovf_trunc.width)   + '\t')
    C_file.write(bin(c_sat_trunc.intvalue)[2:].zfill(c_sat_trunc.width)   + '\t')
    C_file.write(bin(c_sat_round.intvalue)[2:].zfill(c_sat_round.width))

    if(i<sim_length-1):
        A_file.write('\n')
        B_file.write('\n')
        C_file.write('\n')

A_file.close()
B_file.close()
C_file.close()