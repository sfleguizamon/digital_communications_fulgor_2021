/*
 * .SYNOPSIS
 * This Verilog module consists of an Fixed Point adder.
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, July 2nd 2021, 5:15:00 pm
 *    File: fxd_adder.v
 *    Copyright (c) 2021 Santiago F. Leguizamon
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://github.com/sfleguizamon
 */

module fxd_adder 
  #(
    parameter NB_OP_A   = 16                  ,
    parameter NBF_OP_A  = 14                  ,
    parameter NBI_OP_A  = NB_OP_A - NBF_OP_A  ,

    parameter NB_OP_B   = 12                  ,
    parameter NBF_OP_B  = 11                  ,
    parameter NBI_OP_B  = NB_OP_B - NBF_OP_B  ,

    parameter NB_OUT    = 11                  ,
    parameter NBF_OUT   = 10                  ,
    parameter NBI_OUT   = NB_OUT - NBF_OUT
  )
  (
    input   [NB_OP_A-1:0] i_OP_A          ,
    input   [NB_OP_B-1:0] i_OP_B          ,
    output  [NB_OUT-1:0] o_SUM_full       ,
    output  [NB_OUT-1:0] o_SUM_ovf_trunc  ,
    output  [NB_OUT-1:0] o_SUM_sat_trunc  ,
    output  [NB_OUT-1:0] o_SUM_sat_round
  );

  localparam NB_FULL_RES  = (NB_OP_A >= NB_OP_B) ? NB_OP_A + 1 : NB_OP_B + 1 ; // NB of an adder (full resolution) 
  localparam NBF_FULL_RES = (NBF_OP_A >= NBF_OP_B) ? NBF_OP_A : NBF_OP_B ;
  localparam NBI_FULL_RES = NB_FULL_RES - NBF_FULL_RES  ;

  wire signed [NB_OP_A-1:0] a ;
  wire signed [NB_OP_B-1:0] b ;
  wire signed [NB_FULL_RES-1:0] c ;
  wire signed [NB_FULL_RES-1:0] c_rnd ;

  assign a = i_OP_A ;
  assign b = i_OP_B ;
  assign c = (NBF_OP_A > NBF_OP_B) ?                                  // Add with fractional part aligning
             ( a + $signed({b, {(NBF_OP_A - NBF_OP_B){1'b0}}}) ) :
             ( $signed({a, {(NBF_OP_B - NBF_OP_A){1'b0}}}) + b )   ;

  // FULL RESOLUTION

  assign o_SUM_full = c  ;

  // TRUNCATED & OVERFLOW

  assign o_SUM_ovf_trunc = c[(NB_FULL_RES-1) - (NBI_FULL_RES-NBI_OUT) -: NB_OUT] ;

  // TRUNCATED & SATURATED

  assign o_SUM_sat_trunc = (&c[NB_FULL_RES-1 -: NBI_FULL_RES] || ~|c[NB_FULL_RES-1 -: NBI_FULL_RES]) ?
                            c[(NB_FULL_RES-1) - (NBI_FULL_RES-NBI_OUT) -: NB_OUT] :
                            c[(NB_FULL_RES-1)] ? {1'b1, {(NB_OUT-1){1'b0}}} : {1'b0, {(NB_OUT-1){1'b1}}} ;                            

  // ROUNDED & SATURATED

  assign c_rnd = c + {1'b1, {(NBF_FULL_RES-NBF_OUT-1){1'b0}}} ;

  assign o_SUM_sat_round = (&c_rnd[NB_FULL_RES-1 -: NBI_FULL_RES] || ~|c_rnd[NB_FULL_RES-1 -: NBI_FULL_RES]) ?
                          c_rnd[(NB_FULL_RES-1) - (NBI_FULL_RES-NBI_OUT) -: NB_OUT] :
                          c_rnd[(NB_FULL_RES-1)] ? {1'b1, {(NB_OUT-1){1'b0}}} : {1'b0, {(NB_OUT-1){1'b1}}} ;
endmodule