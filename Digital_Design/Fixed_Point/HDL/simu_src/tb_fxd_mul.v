/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, July 2nd 2021, 5:16:24 pm
 *    File: tb_fxd_mul.v
 *    Copyright (c) 2021 Santiago F. Leguizamon
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://github.com/sfleguizamon
 */

`timescale 1ns/100ps

module tb_fxd_mul ();

  localparam NB_OP_A        = 8  ;
  localparam NBF_OP_A       = 6  ;
  localparam NB_OP_B        = 12 ;
  localparam NBF_OP_B       = 11 ;
  localparam NB_OUT_FULL    = NB_OP_A + NB_OP_B ;
  localparam NBF_OUT_FULL   = NBF_OP_A + NBF_OP_B ;
  localparam NB_OUT_TRUNC   = 12 ;
  localparam NBF_OUT_TRUNC  = 11 ;
  localparam NB_OUT_ROUND   = 10 ;
  localparam NBF_OUT_ROUND  = 9 ;

  reg [NB_OP_A-1:0] i_A                   ;
  reg [NB_OP_B-1:0] i_B                   ;
  wire [NB_OUT_FULL-1:0] o_MUL_full        ;
  wire [NB_OUT_TRUNC-1:0] o_MUL_ovf_trunc  ;
  wire [NB_OUT_TRUNC-1:0] o_MUL_sat_trunc  ;
  wire [NB_OUT_ROUND-1:0] o_MUL_sat_round  ;

  integer file_i_A ;
  integer file_i_B ;
  integer file_o_MUL ;

  
  initial begin

    i_A = {NB_OP_A{1'b0}} ;
    i_B = {NB_OP_B{1'b0}} ;

    file_i_A = $fopen("D:/Users/stgol/Github_Public/Fulgor/Digital_Communications_Fulgor_2021/Digital_Design/Fixed_Point/HDL/python_scripts/stimul/op_A.stim","r") ;
    if(file_i_A == 0) begin
      $display("File op_A.stim open error.") ;
      $stop ;
    end

    file_i_B = $fopen("D:/Users/stgol/Github_Public/Fulgor/Digital_Communications_Fulgor_2021/Digital_Design/Fixed_Point/HDL/python_scripts/stimul/op_B.stim","r") ;
    if(file_i_B == 0) begin
      $display("File op_B.stim open error.") ;
      $stop ;
    end

    file_o_MUL = $fopen("D:/Users/stgol/Github_Public/Fulgor/Digital_Communications_Fulgor_2021/Digital_Design/Fixed_Point/HDL/python_scripts/stimul/tb_o_MUL.out","w") ;
    if(file_o_MUL == 0) begin
      $display("File tb_o_MUL.out open error.") ;
      $stop ;
    end

  end

  initial begin

    while (! $feof(file_i_A)) begin
      $fscanf(file_i_A, "%b", i_A) ;
      $fscanf(file_i_B, "%b", i_B) ;
      #10 ;
      $fdisplay(file_o_MUL, "%b\t%b\t%b\t%b", o_MUL_full, o_MUL_ovf_trunc, o_MUL_sat_trunc, o_MUL_sat_round) ;
    end

    $fclose(file_i_A) ;
    $fclose(file_i_B) ;
    $fclose(file_o_MUL) ;
    $finish ;
  end

  fxd_mul
    #(
      .NB_OP_A(NB_OP_A)       ,
      .NBF_OP_A(NBF_OP_A)     ,
      .NB_OP_B(NB_OP_B)       ,
      .NBF_OP_B(NBF_OP_B)     ,
      .NB_OUT(NB_OUT_FULL)    ,
      .NBF_OUT(NBF_OUT_FULL)
    )
    u_fxd_mul_full
    (
      .i_OP_A(i_A) ,
      .i_OP_B(i_B) ,
      .o_MUL_full(o_MUL_full)
    ) ;

  fxd_mul
    #(
      .NB_OP_A(NB_OP_A)       ,
      .NBF_OP_A(NBF_OP_A)     ,
      .NB_OP_B(NB_OP_B)       ,
      .NBF_OP_B(NBF_OP_B)     ,
      .NB_OUT(NB_OUT_TRUNC)   ,
      .NBF_OUT(NBF_OUT_TRUNC)
    )
    u_fxd_ovf_trunc
    (
      .i_OP_A(i_A) ,
      .i_OP_B(i_B) ,
      .o_MUL_ovf_trunc(o_MUL_ovf_trunc)
    ) ;
  
  fxd_mul
    #(
      .NB_OP_A(NB_OP_A)       ,
      .NBF_OP_A(NBF_OP_A)     ,
      .NB_OP_B(NB_OP_B)       ,
      .NBF_OP_B(NBF_OP_B)     ,
      .NB_OUT(NB_OUT_TRUNC)   ,
      .NBF_OUT(NBF_OUT_TRUNC)
    )
    u_fxd_sat_trunc
    (
      .i_OP_A(i_A) ,
      .i_OP_B(i_B) ,
      .o_MUL_sat_trunc(o_MUL_sat_trunc)
    ) ;

  fxd_mul
    #(
      .NB_OP_A(NB_OP_A)       ,
      .NBF_OP_A(NBF_OP_A)     ,
      .NB_OP_B(NB_OP_B)       ,
      .NBF_OP_B(NBF_OP_B)     ,
      .NB_OUT(NB_OUT_ROUND)   ,
      .NBF_OUT(NBF_OUT_ROUND)
    )
    u_fxd_sat_round
    (
      .i_OP_A(i_A) ,
      .i_OP_B(i_B) ,
      .o_MUL_sat_round(o_MUL_sat_round)
    ) ;

endmodule



