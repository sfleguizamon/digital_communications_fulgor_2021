`timescale 1ns/100ps

module tb_sum ();

  localparam NB_OP_A        = 16 ;
  localparam NBF_OP_A       = 14 ;
  localparam NB_OP_B        = 12 ;
  localparam NBF_OP_B       = 11 ;
  localparam NB_OUT_FULL    = 17 ;
  localparam NBF_OUT_FULL   = 14 ;
  localparam NB_OUT_TRUNC   = 11 ;
  localparam NBF_OUT_TRUNC  = 10 ;
  localparam NB_OUT_ROUND   = 9 ;
  localparam NBF_OUT_ROUND  = 8 ;

  reg [NB_OP_A-1:0] i_A                   ;
  reg [NB_OP_B-1:0] i_B                   ;
  wire [NB_OUT_FULL-1:0] o_SUM_full        ;
  wire [NB_OUT_TRUNC-1:0] o_SUM_ovf_trunc  ;
  wire [NB_OUT_TRUNC-1:0] o_SUM_sat_trunc  ;
  wire [NB_OUT_ROUND-1:0] o_SUM_sat_round  ;

  initial begin
    i_A = 16'b0101101010011000 ;
    i_B = 12'b001011100010 ;
    #100 $finish;
  end

  fxd_adder
    #(
      .NB_OP_A(NB_OP_A)       ,
      .NBF_OP_A(NBF_OP_A)     ,
      .NB_OP_B(NB_OP_B)       ,
      .NBF_OP_B(NBF_OP_B)     ,
      .NB_OUT(NB_OUT_FULL)    ,
      .NBF_OUT(NBF_OUT_FULL)
    )
    u_fxd_adder_full
    (
      .i_OP_A(i_A) ,
      .i_OP_B(i_B) ,
      .o_SUM_full(o_SUM_full)
    ) ;

endmodule