# TP4: Fixed Point

La operación con números representados en **Punto Flotante** conlleva grandes costos de hardware, en cuanto a área y potencia. En ese sentido, la representación en **Punto Fijo** de un número presenta ventajas por su simplicidad. Sin embargo, una vez definida la ubicación del punto decimal y el tamaño en bits de la variable, esta representación está limitada en rango (máximo y mínimo valor representable) y precisión (debido a la cantidad de dígitos fraccionales representables). Si el valor a representar cae por fuera del rango, se produce *overflow*, problema que puede ser solucionado aritméticamente (o al menos reducir al máximo posible su efecto negativo) mediante la *saturación*. Por otra parte, si el número a representar tiene una cantidad de dítitos mayor a la representable, se puede realizar un *redondeo*.

En este trabajo práctico se aplican los conceptos de aritmética de punto fijo. Este trabajo se divide en dos partes principales.

* Análisis de cuantización aplicado a transceptor QPSK elemental (Python3 scripts)
* Descripción en Verilog de un sumador y un multiplicador de variables en punto fijo.

## Análisis de cuantización aplicado a transceptor QPSK elemental

### Simulación del transceptor QPSK elemental

El primer paso de este análisis consistió en escribir un script de Python 3, que consiste en un [simulador de un transceptor QPSK elemental](Scripts/QPSK_transceiver_elemental.py).
Se utilizó el módulo [DSPtools](Scripts/tool/DSPtools.py), para calcular los coeficientes del *pulse-shaping filter*
y se agregaron algunas funciones nuevas al módulo:

* `upsample()` y `downsample()` que se utilizan para sobremuestrear y decimar un vector.
* `fxd_quantization()` que realiza la cuantización en punto fijo de la respuesta de un filtro.

Para el caso de `fxd_quantization()`, se utilizó el módulo [_fixedInt](Scripts/tool/_fixedInt.py), también provisto por Fundación Fulgor. A este sólo se le agregó el método `getRange` a la clase `DeFixedInt`. Este método devuelve una lista con los valores máximos y mínimos que puede tomar el objeto de la clase, para una representación definida.

Retomando el desarrollo del [script](Scripts/QPSK_transceiver_elemental.py) mencionado, en él se simuló el comportamiento de un transceptor QPSK elemental, realizando a su vez la comparación de los resultados obtenidos al utilizar un pulse-shaping filter (Raised cosine filter) con distintos valores de *rolloff*. A continuación, se presentan las respuestas al impulso de cada uno de los filtros, y sus correspondientes respuestas en frecuencia.

![](Scripts/plots/Filter_impulse_responses.svg)
![](Scripts/plots/Filter_frequency_responses.svg)

Luego se generaron los símbolos a transmitir, se sobremuestrearon y se convolucionaron con cada uno de los filtros. Los resultados, para las componentes en fase y en cuadratura se presentan a continuación.

![](Scripts/plots/Interpolated_signal_I.svg)
![](Scripts/plots/Interpolated_signal_Q.svg)

Por último, luego de submuestrear los símbolos, se relizó el diagrama de constelación de los mismos, como se puede observar en la siguiente figura.

![](Scripts/plots/Constellation_diagrams.svg)

### Cuantización del pulse-shaping filter

Luego de evaluar el simulador, se realizaron distintas cuantizaciones de punto fijo sobre los componentes de la respuesta al impulso del pulse-shaping filter.
Aquí es donde se utilizó la función `fxd_quantization()` agregada al módulo DSPTools. El script correspondiente puede encontrarse [aquí.](Scripts/fxd_filter_quatization_analysis.py)
En las siguientes subsecciones, se realiza la comparación de cada una de las cuantizaciones realizadas superponiendo la respuesta obtenida mediante representación en punto flotante y representación en punto fijo (con truncado y con redondeo).

#### **Punto fijo S(8, 7)**

**Respuestas al impulso**

![](Scripts/plots/Impulse_responses_fxd_S(8-7).svg)

**Respuestas en frecuencia**

![](Scripts/plots/Frequency_responses_fxd_S(8-7).svg)

**Señales (símbolos) interpolados con el filtro**

![](Scripts/plots/Interpolated_signals_I_fxd_S(8-7).svg)
![](Scripts/plots/Interpolated_signals_Q_fxd_S(8-7).svg)

**Diagramas de constalación**

![](Scripts/plots/Constellation_diagrams_fxd_S(8-7).svg)

#### **Punto fijo S(6, 4)**

**Respuestas al impulso**

![](Scripts/plots/Impulse_responses_fxd_S(6-4).svg)

**Respuestas en frecuencia**

![](Scripts/plots/Frequency_responses_fxd_S(6-4).svg)

**Señales (símbolos) interpolados con el filtro**

![](Scripts/plots/Interpolated_signals_I_fxd_S(6-4).svg)
![](Scripts/plots/Interpolated_signals_Q_fxd_S(6-4).svg)

**Diagramas de constalación**

![](Scripts/plots/Constellation_diagrams_fxd_S(6-4).svg)

#### **Punto fijo S(3, 2)**

**Respuestas al impulso**

![](Scripts/plots/Impulse_responses_fxd_S(3-2).svg)

**Respuestas en frecuencia**

![](Scripts/plots/Frequency_responses_fxd_S(3-2).svg)

**Señales (símbolos) interpolados con el filtro**

![](Scripts/plots/Interpolated_signals_I_fxd_S(3-2).svg)
![](Scripts/plots/Interpolated_signals_Q_fxd_S(3-2).svg)

**Diagramas de constalación**

![](Scripts/plots/Constellation_diagrams_fxd_S(3-2).svg)

Por último, el [script](Scripts/fxd_filter_quatization_analysis.py) calcula la SNR de la señal transmitida, y genera un [reporte](Scripts/reports/SNR_report.csv) en formato *csv*, donde presenta la SNR propia de realizar una cuantización en punto fijo S(NB, NBF), y diferenciando también los casos de redondeo y truncado.

## Descripción en Verilog de un sumador y un multiplicador de variables en punto fijo.

La segunda parte de este trabajo consiste en la descripción, en Verilog, de un sumador ([fxd_adder.v](HDL/dsgn_src/fxd_adder.v)) y un multiplicador ([fxd_mul.v](HDL/dsgn_src/fxd_mul.v)) de variables representadas en punto fijo. Cada uno de los módulos descriptos tiene la posibilidad de entregar el resultado en los formatos Full-resolution, S11.10 con overflow y truncado, S11.10 con saturación y truncado, y S9.8 con saturación y redondeo.

Para la simulación de estos módulos, se realizaron los testbenches [tb_fxd_adder.v](HDL/simu_src/tb_fxd_adder.v) y [tb_fxd_mul.v](HDL/simu_src/tb_fxd_mul.v).

Para la generación de los estímulos, es decir los operandos cuantizados, se realizaron los scripts de python3 [fxd_adder_stimulus_file_gen.py](HDL/python_scripts/fxd_adder_stimulus_file_gen.py) y [fxd_mul_stimulus_file_gen.py](HDL/python_scripts/fxd_mul_stimulus_file_gen.py), que generan [archivos](HDL/python_scripts/stimul) que contienen una cantidad (que puede ser cambiada) de números representados en punto fijo, que luego serán utilizados como entrada de los módulos. También generan [archivos](HDL/python_scripts/stimul) con el resultado de las operaciones suma y multiplicación, en los mismos formatos de salida que los que permiten los módulos descriptos en Verilog.

Los testbenches mencionados, leen los archivos generados por los scripts, y almacenan el valor de las señales de salida del módulo en los archivos [tb_o_SUM.out](HDL/python_scripts/stimul/tb_o_SUM.out) y [tb_o_MUL.out](HDL/python_scripts/stimul/tb_o_MUL.out). Estos archivos fueron comparados, utilizando otro [script](HDL/python_scripts/vector_comp.py) y generando un [reporte](HDL/python_scripts/reports/report.txt) que contienen los índices de las entradas/salidas en las que el resultado obtenido del módulo en Verilog difiere del cálculo de Python.