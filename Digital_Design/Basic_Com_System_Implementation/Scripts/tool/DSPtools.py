import numpy as np
import matplotlib.pyplot as plt
from tool._fixedInt import *

def rcosine(beta, Tbaud, oversampling, Nbauds, Norm):
    """ Respuesta al impulso del pulso de caida cosenoidal """
    t_vect = np.arange(-0.5*Nbauds*Tbaud, 0.5*Nbauds*Tbaud, float(Tbaud)/oversampling)

    y_vect = []
    for t in t_vect:
        y_vect.append(np.sinc(t/Tbaud)*(np.cos(np.pi*beta*t/Tbaud)/
                                                (1-(4.0*beta*beta*t*t/(Tbaud*Tbaud)))))

    y_vect = np.array(y_vect)
    if(Norm):
        return (t_vect, y_vect/y_vect.sum())
    else:
        return (t_vect,y_vect)
    
def eyediagram(data, n, offset, period):
    span     = 2*n
    segments = int(len(data)/span)
    xmax     = (n-1)*period
    xmin     = -(n-1)*period
    x        = list(np.arange(-n,n,)*period)
    xoff     = offset

    plt.figure(figsize=[5, 5])
    for i in range(0,segments-1):
        plt.plot(x, data[int(i*span+xoff):int((i+1)*span+xoff)],'r')
        plt.grid(True)

    plt.xlim(xmin, xmax)

def fxd_quantization(h, NB, NBF, signedMode = True, roundMode = False, saturateMode = True):
    """
    Quantizate the taps of the impluse response of a filter, by using fixed point representation.

    Uses arrayFixedInt class from _fixedInt module.
        
    PARAMETERS:
        h : array_like
            Filter taps array.
        NB : int
            Total number of bits for the fixed point implementation.
        NBF : int
            Total number of bits for the fixed point implementation.
        signedMode : bool
            True == signed.
            False == unsigned.
        roudnMode : bool
            True == round.
            False == truncate.
        saturatedMode : bool
            True == saturate.
            False == wrap (overflow).

    RETURNS:
        h_q : array_like.
              Upsampled signal.
    """

    if (signedMode):
        signedMode = 'S'
    else:
        signedMode = 'U'

    if (roundMode):
        roundMode = 'round'
    else:
        roundMode = 'trunc'

    if (saturateMode):
        saturateMode = 'saturate'
    else:
        saturateMode = 'wrap'

    h_fxd = arrayFixedInt(int(NB), int(NBF), h, signedMode, roundMode, saturateMode)
    h_q = []
    for i in range(len(h)):
        h_q.append(h_fxd[i].fValue)
    
    return h_q

def upsample(X, N):
    """
    Upsample input signal.

    Upsample(X, N) upsamples input signal X by inserting N-1 zeros between input samples.
        
    PARAMETERS:
        X : array_like
            Input array.
        N : int
            Upsampling factor.

    RETURNS:
        XUP : array_like.
              Upsampled signal.
    """
    XUP = np.zeros(int(N) * len(X))
    XUP[0:len(XUP):int(N)] = X

    return XUP

def downsample(X, N, PHASE=0):
    """
    Downsample input signal.

    Downsample(X, N) downsamples input signal X by keeping every N-th sample starting with the first.
    Downsample(X, N, PHASE) specifies an optional sample offset.
    PARAMETERS:
        X : array_like
            Input array.
        N : int
            Upsampling factor.
        PHASE

    RETURNS:
        XDN : array_like.
              Downsampled signal.
    """
    XDN = X[PHASE:len(X)-PHASE:int(N)]

    return XDN