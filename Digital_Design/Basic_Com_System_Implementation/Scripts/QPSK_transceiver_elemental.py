'''
.SYNOPSIS
<Overview of script>
.DESCRIPTION
<Brief description of script>
.PARAMETER <Parameter_Name>
<Brief description of parameter input required. Repeat this attribute if required>
.INPUTS
<Inputs if any, otherwise state None>
.OUTPUTS
<Outputs if anything is generated>
.NOTES
   Version:        0.1
   Author:         Santiago F. Leguizamon
   Email:          stgoleguizamon@gmail.com
   Creation Date:  Tuesday, August 3rd 2021, 3:31:21 pm
   File: QPSK_transceiver_elemental.py
   Copyright (c) 2021 Santiago F. Leguizamon
HISTORY:
Date      	          By	Comments
----------	          ---	----------------------------------------------------------

.LINK
   https://github.com/sfleguizamon

.COMPONENT 
 Required Modules: numpy, matplotlib, scipy, ff_model, DSPtools and _fixedInt.
    DSPtools, ff_model and _fixedInt modules were provided by Fundacion Fulgor.
'''

# WARNING: selected number of taps must be even!
#

import os
from matplotlib.colors import Colormap
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.fromnumeric import size
from scipy.fft import fft, fftfreq, fftshift
from tool._fixedInt import *
from tool.DSPtools import *
from tool.ff_model import *



# #########################################################################
#                           Simulation settings
# #########################################################################

# REPORTS PARAMETERS

plots_enable        = True
plots_show          = True
plots_path          = 'Scripts/plots/'
reports_path        = 'Scripts/reports/'
filter_taps_path    = 'HDL/dsgn_src/'

NB_FULL_RES         = 15    # Used to compare to RTL signal. Must be calculated considering
NBF_FULL_RES        = 7     # all multipliers and adders used in convolution.

# GENERAL PARAMETERS
BR              = 1e9               # Symbol rate
T               = 1/BR              # Time between symbols
oversampling    = 4                 # Oversampling rate
sim_length      = 2000              # Simulation length (bits quantity)
Ts              = T/oversampling    # Sampling period (1/fs)
NFFT            = 1024*8            # FFT points

# FILTERING PARAMETERS
rolloff         = 0.5               # Raised cosine filter rolloff
Nzc             = 6                 # Number of zero crossings
Norm            = False             # Filter amplitude normalization enable
                                    # (normalization on freq domain)
RTL_filter_sim  = True              # If true, filter and oversampling is performed 
                                    # using a RTL approach

# QUANTIZATION PARAMETERS (Fixed point)

NB  = 8
NBF = 7

# PRBS PARAMETERS

PRBS_ord = 9
PRBS_seedI = [1, 1, 0, 1, 0, 1, 0, 1, 0] # Must be != 0
PRBS_seedQ = [1, 1, 1, 1, 1, 1, 1, 1, 0] # Must be != 0

# #########################################################################
#               PRBS9 generator modeling using flipflops
# #########################################################################

d_LFSR9_I = ff(9)
d_LFSR9_I.i = PRBS_seedI
d_LFSR9_I.run_clock()

d_LFSR9_Q = ff(9)
d_LFSR9_Q.i = PRBS_seedQ
d_LFSR9_Q.run_clock()

symbolsI = []
symbolsQ = []

for clock in range(sim_length):

    d_LFSR9_I[0].i = d_LFSR9_I[4].o ^ d_LFSR9_I[8].o
    d_LFSR9_Q[0].i = d_LFSR9_Q[4].o ^ d_LFSR9_Q[8].o

    for i in range(1, PRBS_ord):
        d_LFSR9_I[i].i = d_LFSR9_I[i-1].o
        d_LFSR9_Q[i].i = d_LFSR9_Q[i-1].o

    symbolsI.append(d_LFSR9_I[PRBS_ord-1].o)
    symbolsQ.append(d_LFSR9_Q[PRBS_ord-1].o)

    d_LFSR9_I.run_clock()
    d_LFSR9_Q.run_clock()

# #########################################################################
#                                 Mapper
# #########################################################################

mapped_data_I = []
mapped_data_Q = []

for i in range(len(symbolsI)):
    mapped_data_I.append(symbolsI[i]*2-1)
    mapped_data_Q.append(symbolsQ[i]*2-1)

# #########################################################################
#                             Filter creation
# #########################################################################

(t, h) = rcosine(rolloff, T, oversampling, Nzc, Norm)           # Impulse responses generation
h_fxd_round = fxd_quantization(h, NB, NBF, True, True, True)    # Quantization S(NB, NBF)

mod             = fftshift( np.log10( abs( fft(h, NFFT) ) ) )   # Filter frequency response
mod_fxd_round   = fftshift( np.log10( abs( fft(h_fxd_round, NFFT) ) ) )
freq            = fftshift( fftfreq(NFFT, Ts) ) / 1e9           # Convertido a GHz


# #########################################################################
#                     Symbols and filter convolution 
# #########################################################################

if(RTL_filter_sim):

    Nffs_conv = int(len(h_fxd_round)/oversampling) # Number of flipflops for polyphasic decomposed FIR

    shift_regs_conv_I = ff(Nffs_conv)
    shift_regs_conv_Q = ff(Nffs_conv)

    shift_reg_en = 0

    taps_index = 0

    tx_out_I = []
    tx_out_Q = []

    shift_regs_conv_I[0].i = 0
    shift_regs_conv_Q[0].i = 0

    for clock in range(int(sim_length)):

        if (clock % int(len(h_fxd_round)/Nffs_conv)) == 0:
            if clock == 0:
                shift_regs_conv_I[0].i = mapped_data_I[clock]
                shift_regs_conv_Q[0].i = mapped_data_Q[clock]
            else:
                shift_regs_conv_I[0].i = mapped_data_I[clock-(oversampling-1)]
                shift_regs_conv_Q[0].i = mapped_data_Q[clock-(oversampling-1)]
            for index in range(1, Nffs_conv):
                shift_regs_conv_I[index].i = shift_regs_conv_I[index-1].o
                shift_regs_conv_Q[index].i = shift_regs_conv_Q[index-1].o
            shift_regs_conv_I.run_clock()
            shift_regs_conv_Q.run_clock()

        result_I = 0
        result_Q = 0
        step = 0
        for mul_index in range(Nffs_conv): # Multiplications
            result_I = result_I + shift_regs_conv_I[mul_index].o * h_fxd_round[taps_index + step]
            result_Q = result_Q + shift_regs_conv_Q[mul_index].o * h_fxd_round[taps_index + step]
            step = step + int(len(h_fxd_round)/Nffs_conv)
        tx_out_I.append(result_I)
        tx_out_Q.append(result_Q)
        taps_index = taps_index + 1
        if taps_index == int(len(h_fxd_round)/Nffs_conv):
            taps_index = 0

    tx_I_h_fxd_round = tx_out_I
    tx_Q_h_fxd_round = tx_out_Q

else:
    mapped_data_I_up = upsample(mapped_data_I, oversampling)
    mapped_data_Q_up = upsample(mapped_data_Q, oversampling)
    tx_I_h_fxd_round = np.convolve(mapped_data_I_up, h_fxd_round, 'valid')
    tx_Q_h_fxd_round = np.convolve(mapped_data_Q_up, h_fxd_round, 'valid')


# #########################################################################
#                               Downsampling
# #########################################################################

rx_I_h1_fxd_round = [[], [], [], []]
rx_Q_h1_fxd_round = [[], [], [], []]

phase = [0, 1, 2, 3]
guard = 50 # To eliminate extra zeros from convolution

for i in range(len(phase)):
    rx_I_h1_fxd_round[i] = downsample(tx_I_h_fxd_round[guard:], oversampling, phase[i])
    rx_Q_h1_fxd_round[i] = downsample(tx_Q_h_fxd_round[guard:], oversampling, phase[i])

# #########################################################################
#                       Reports files generation
# #########################################################################

# Filter taps lut file generation

tap = DeFixedInt(NB, NBF, 'S', 'round', 'saturate')

TAPS_file = open(filter_taps_path + 'filter_lut.v', 'w')

TAPS_file.write("wire signed [NB_TAPS-1:0] coeffs_lut [N_TAPS-1:0] ;\n\n")

for i in range(len(h_fxd_round)):
    tap.value = h_fxd_round[i]
    TAPS_file.write("assign coeffs_lut[" + str(i) + "] = " + str(NB) + "'b" + bin(tap.intvalue)[2:].zfill(tap.width) + ";")
    if(i<len(h_fxd_round)-1):
            TAPS_file.write('\n')

TAPS_file.close()

# PRBS9 output file

PRBS9_file = open(reports_path + 'PRBS9_out.txt', 'w')

PRBS9_file.write('PRBS9 I-seed = {}\tPRBS9 Q-seed = {}\n'.format(PRBS_seedI, PRBS_seedQ))

for i in range(len(symbolsI)):
    PRBS9_file.write(str(symbolsI[i]) + '\t' + str(symbolsQ[i]))

    if(i<len(symbolsI)-1):
            PRBS9_file.write('\n')

PRBS9_file.close()

# Tx signals output file

tx_signal = DeFixedInt(NB_FULL_RES, NBF_FULL_RES, 'S', 'round', 'saturate')

Tx_out_file = open(reports_path + 'Tx_out.txt', 'w')
Tx_out_file.write('In-phase\tQuadrature\n')

for i in range(len(tx_I_h_fxd_round)):
    tx_signal.value = tx_I_h_fxd_round[i]
    Tx_out_file.write(bin(tx_signal.intvalue)[2:].zfill(tx_signal.width) + '\t')
    tx_signal.value = tx_Q_h_fxd_round[i]
    Tx_out_file.write(bin(tx_signal.intvalue)[2:].zfill(tx_signal.width))
    if(i<len(tx_I_h_fxd_round)-1):
            Tx_out_file.write('\n')

Tx_out_file.close()

# Rx signals output file
# File contains every phase

rx_signal = DeFixedInt(NB_FULL_RES, NBF_FULL_RES, 'S', 'round', 'saturate')

rx_out_file = open(reports_path + 'Rx_bits.txt', 'w')
rx_out_file.write('In-phase\t\t\t\t\t\t\t\t\t\tQuadrature\n')
for i in phase:
    rx_out_file.write(str(phase[i]) + '\t'*3)
for i in phase:
    rx_out_file.write(str(phase[i]) + '\t'*3)
rx_out_file.write('\n')


samples_size = min(len(rx_I_h1_fxd_round[0]), len(rx_I_h1_fxd_round[1]), len(rx_I_h1_fxd_round[2]), len(rx_I_h1_fxd_round[3]))

for i in range(samples_size):
    for j in range(len(phase)):
        rx_out_file.write(str(rx_I_h1_fxd_round[j][i]) + '\t')
    for j in range(len(phase)):
        rx_out_file.write(str(rx_Q_h1_fxd_round[j][i]) + '\t')   
    if(i<samples_size-1):
                rx_out_file.write('\n')

Tx_out_file.close()

# #########################################################################
#                                   Plots
# #########################################################################

if (plots_enable):
    # Filter impulse responses plot

    plt.figure(figsize = [5, 5]) 
    plt.plot(h, '-b', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff))
    plt.plot(h_fxd_round, '-r', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff, NB, NBF))
    plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
    plt.xlim(0, Nzc*oversampling-1); plt.title('Impulse responses')
    plt.savefig(plots_path + 'Impulse_responses_fxd_S({}-{}).svg'.format(NB, NBF), format = 'svg')

    # Filter freq responses plots

    plt.figure(figsize = [5, 5]) 
    plt.plot(freq, mod, '-b', linewidth = 1, label = r'$\beta={},\ Float$'.format(rolloff))
    plt.plot(freq, mod_fxd_round, '-r', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff, NB, NBF))
    plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Frequency [GHz]'); plt.ylabel('Magnitude [dB]')
    plt.xlim(-2, 2); plt.ylim(-5, 1); plt.title('Frequency responses')
    plt.savefig(plots_path + 'Frequency_responses_fxd_S({}-{}).svg'.format(NB, NBF), format = 'svg')

    # Tx output eyediagrams

    eyediagram(tx_I_h_fxd_round,4,8,1) 
    plt.title("Eye diagram (In-phase component)")
    plt.savefig(plots_path + 'Tx_out_eye_I.svg', format = 'svg')
    eyediagram(tx_Q_h_fxd_round,4,8,1)
    plt.title("Eye diagram (Quadrature component)")
    plt.savefig(plots_path + 'Tx_out_eye_Q.svg', format = 'svg')

    # Interpolated signals plots 

    plt.figure(figsize = [5, 5]) # In-Phase components
    plt.plot(tx_I_h_fxd_round, '-r', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff, NB, NBF))
    plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
    plt.xlim(1480,1680); plt.title('In-Phase signal component')
    plt.savefig(plots_path + 'Tx_out_I.svg', format = 'svg')

    plt.figure(figsize = [5, 5]) # Quadrature components
    plt.plot(tx_Q_h_fxd_round, '-r', linewidth = 1, label = r'$\beta={},\ S({},{}),\ Round$'.format(rolloff, NB, NBF))
    plt.grid(); plt.legend(loc = 'upper right', fontsize = 'x-small'); plt.xlabel('Samples'); plt.ylabel('Amplitude')
    plt.xlim(620,820); plt.title('Quadrature signal component')
    plt.savefig(plots_path + 'Tx_out_Q.svg', format = 'svg')

    # Constellation diagrams

    plt.figure(figsize = [10, 10]) 
    for i in phase:
        plt.subplot(2, 2, (i+1))
        plt.scatter(rx_I_h1_fxd_round[i], rx_Q_h1_fxd_round[i], c = 'r', marker='o',label = r'$\beta={},\ S({},{}),\ Round,\ Phase = {}$'.format(rolloff, NB, NBF, i))
        plt.grid(); plt.legend(loc = 'upper center', fontsize = 'small'); plt.xlabel('In-Phase'); plt.ylabel('Quadrature')
    plt.suptitle('Constellation diagrams using different phases', fontsize = 'xx-large', y = 0.93)
    plt.savefig(plots_path + 'Rx_constellation_diagrams.svg', format = 'svg')
    
    if(plots_show):
        plt.show(block = True)