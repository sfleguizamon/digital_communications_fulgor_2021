/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, August 6th 2021, 6:27:50 pm
 *    File: tb_top.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */

`timescale 1ns/100ps

module tb_top ();

  localparam N_TAPS       = 24            ;
  localparam NB_TAPS      = 8             ;
  localparam NBF_TAPS     = 7             ;
  localparam os_rate      = 4             ;
  localparam MAPPER_EN    = 1             ;
  localparam PRBS9_seed_I = 9'b101010101  ;
  localparam PRBS9_seed_Q = 9'b011111111  ;

  localparam NB_i_sw  = 4;
  localparam NB_o_led = 4;
  localparam NB_TX_OUT = (2+NB_TAPS) + (N_TAPS/os_rate - 1)  ;

  reg                       clk               ;
  reg                       rst               ;
  reg   [NB_i_sw   - 1 : 0] i_sw              ;
  wire  [NB_o_led  - 1 : 0] o_led             ;
  wire  [NB_TX_OUT - 1 : 0] tx_signal_I       ;
  wire  [NB_TX_OUT - 1 : 0] tx_signal_Q       ;  
  wire  [64 - 1 : 0       ] BitErrorCounter_I ;
  wire  [64 - 1 : 0       ] BitErrorCounter_Q ;

  top
    #(
      .N_TAPS       ( N_TAPS        ) ,
      .NB_TAPS      ( NB_TAPS       ) ,
      .NBF_TAPS     ( NBF_TAPS      ) ,
      .os_rate      ( os_rate       ) ,
      .MAPPER_EN    ( MAPPER_EN     ) ,
      .PRBS9_seed_I ( PRBS9_seed_I  ) ,
      .PRBS9_seed_Q ( PRBS9_seed_Q  )
    )
    u_top
    (
      .i_clock    ( clk     ) ,
      .i_reset    ( rst     ) ,
      .i_sw       ( i_sw    ) ,
      .o_led      ( o_led   )
    ) ;

  assign BitErrorCounter_I = u_top.u_prbs_checker_I.errors_count  ;
  assign BitErrorCounter_Q = u_top.u_prbs_checker_Q.errors_count  ;
  assign tx_signal_I = u_top.tx_out_I ;
  assign tx_signal_Q = u_top.tx_out_Q ;

  initial begin
    clk                 = 1'b0  ;
    rst                 = 1'b1  ;
    i_sw                = 4'b0  ;
    #1        rst       = 1'b0  ;
              i_sw[0]   = 1'b1  ; // Enabling Tx
              i_sw[1]   = 1'b1  ; // Enabling Rx
              i_sw[3:2] = 2'b00 ; // Selecting downsampler phase (10 is the worst case)
    #1000000  $finish           ;
  end

  always #5 clk = ~clk;

endmodule