/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Wednesday, August 4th 2021, 4:14:52 pm
 *    File: tb_PRBS9.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */




`timescale 1ns/100ps

module tb_PRBS9 ();

  localparam seed = 9'b101010101  ;

  reg   clk       ;
  reg   rst       ;
  reg   enable    ;
  wire  PRBS9_out ;

  PRBS9
    #(
      .seed ( seed )
    )
    u_PRBS9
    (
      .i_clock  ( clk       ) ,
      .i_reset  ( rst       ) ,
      .i_enable ( enable    ) ,
      .o_PRBS9  ( PRBS9_out )
    ) ;
  
  initial begin
    clk           = 0 ;
    enable        = 0 ;
    rst           = 1 ;
    #1    rst     = 0 ;
    #1    enable  = 1 ;
    #2562 $finish     ;
  end

  always #5 clk = ~clk  ; 
  
endmodule