/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Thursday, August 5th 2021, 9:03:17 pm
 *    File: tb_FIR_Filter.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */




`timescale 1ns/100ps

module tb_FIR_Filter ();

  localparam N_TAPS    = 24   ; // Number of filter taps
  localparam NB_TAPS   = 8    ; // N of bits used in fixed point representation
  localparam NBF_TAPS  = 7    ;
  localparam os_rate   = 4    ; // Oversampling rate. Must be even
  localparam MAPPER_EN = 0    ;

  reg                                                 clk           ;
  reg                                                 rst           ;
  reg                                                 enable        ;
  reg                                                 mapper_en     ;
  reg                                                 signalBit_in  ;
  wire [(2+NB_TAPS) + (N_TAPS/os_rate - 1) - 1 : 0 ]  conv_out      ;

  FIR_Filter
    #(
      .N_TAPS   ( N_TAPS    ) ,
      .NB_TAPS  ( NB_TAPS   ) ,
      .NBF_TAPS ( NBF_TAPS  ) ,
      .os_rate  ( os_rate   )
    )
    u_FIR_Filter
    (
      .i_clock          ( clk          ) ,
      .i_reset          ( rst          ) ,
      .i_enable         ( enable       ) ,
      .i_signal_bit_in  ( signalBit_in ) ,
      .i_mapper_en      ( mapper_en    ) ,
      .o_conv           ( conv_out     )
    ) ;

  initial begin
    mapper_en           = MAPPER_EN ;
    clk                 = 0         ;
    enable              = 0         ;
    rst                 = 1         ;
    signalBit_in        = 0         ;
    #1    rst           = 0         ;
    #1    enable        = 1         ;
    #1    signalBit_in  = 1'b1      ;
    #45   signalBit_in  = 1'b0      ;
    #250  $finish                   ;
  end

  always #5 clk = ~clk; 
  
endmodule