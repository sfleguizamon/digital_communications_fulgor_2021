/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, August 6th 2021, 5:37:54 pm
 *    File: top.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */




module top #(
  parameter N_TAPS        = 24            ,
  parameter NB_TAPS       = 8             ,
  parameter NBF_TAPS      = 7             ,
  parameter os_rate       = 4             ,
  parameter MAPPER_EN     = 1             ,
  parameter PRBS9_seed_I  = 9'b101010101  ,
  parameter PRBS9_seed_Q  = 9'b011111111
) (
  input               i_clock ,
  input               i_reset ,
  input   [4 - 1 : 0] i_sw    ,
  output  [4 - 1 : 0] o_led
);

  localparam NB_i_sw    = 4                                   ;
  localparam NB_o_led   = 4                                   ;
  localparam NB_TX_OUT  = (2+NB_TAPS) + (N_TAPS/os_rate - 1)  ;

  wire  [NB_TX_OUT - 1 : 0] tx_out_I      ;
  wire  [NB_TX_OUT - 1 : 0] tx_out_Q      ;
  wire                      PRBS9_enable  ;
  wire                      mapper_en     ;
  wire                      PRBS9_out_I   ;
  wire                      PRBS9_out_Q   ;
  wire                      slicer_out_I  ;
  wire                      slicer_out_Q  ;
  wire                      symb_I        ;
  wire                      symb_Q        ;
  wire                      checker_I     ;
  wire                      checker_Q     ;

  control
    #(
      .os_rate    ( os_rate   ) ,
      .MAPPER_EN  ( MAPPER_EN )
    )
    u_control
    (
      .i_clock          ( i_clock       ) ,
      .i_reset          ( i_reset       ) ,
      .o_PRBS9_enable   ( PRBS9_enable  ) ,
      .o_mapper_enable  ( mapper_en     )
    ) ;

  // In-Phase signal path related architecture 

  PRBS9
    #(
      .seed ( PRBS9_seed_I )
    )
    u_PRBS9_I
    (
      .i_clock  ( i_clock                 ) ,
      .i_reset  ( i_reset                 ) ,
      .i_enable ( PRBS9_enable & i_sw[0]  ) ,
      .o_PRBS9  ( PRBS9_out_I             )
    ) ;

  FIR_Filter
    #(
      .N_TAPS   ( N_TAPS   )  ,
      .NB_TAPS  ( NB_TAPS  )  ,
      .NBF_TAPS ( NBF_TAPS )  ,
      .os_rate  ( os_rate  )
    )
    u_FIR_Filter_I
    (
      .i_clock          ( i_clock     ) ,
      .i_reset          ( i_reset     ) ,
      .i_enable         ( i_sw[0]     ) ,
      .i_signal_bit_in  ( PRBS9_out_I ) ,
      .i_mapper_en      ( mapper_en   ) ,
      .o_conv           ( tx_out_I    )
    ) ;

  slicer
    #()
    u_slicer_I
    (
      .i_sample_sign  ( tx_out_I[NB_TX_OUT-1] ) ,
      .o_bit          ( slicer_out_I          )
    ) ;

  downsampler
    #()
    u_downsampler_I
    (
      .i_sample       ( slicer_out_I  ) ,
      .i_clock        ( i_clock       ) ,
      .i_reset        ( i_reset       ) ,
      .i_enable       ( PRBS9_enable  ) ,
      .i_phase_select ( i_sw[3:2]     ) ,
      .o_symb         ( symb_I        )
    ) ;

  prbs_checker
    #()
    u_prbs_checker_I
    (
      .i_clock    ( i_clock     ) ,
      .i_reset    ( ~i_reset    ) ,
      .i_enable   ( i_sw[1]     ) ,
      .i_ref_bit  ( PRBS9_out_I ) ,
      .i_bit      ( symb_I      ) ,
      .o_led      ( checker_I   )
    ) ;

  // Quadrature signal path related architecture 

  PRBS9
    #(
      .seed ( PRBS9_seed_Q )
    )
    u_PRBS9_Q
    (
      .i_clock  ( i_clock                 ) ,
      .i_reset  ( i_reset                 ) ,
      .i_enable ( PRBS9_enable & i_sw[0]  ) ,
      .o_PRBS9  ( PRBS9_out_Q             )
    ) ;

  FIR_Filter
    #(
      .N_TAPS   ( N_TAPS   )  ,
      .NB_TAPS  ( NB_TAPS  )  ,
      .NBF_TAPS ( NBF_TAPS )  ,
      .os_rate  ( os_rate  )
    )
    u_FIR_Filter_Q
    (
      .i_clock          ( i_clock     ) ,
      .i_reset          ( i_reset     ) ,
      .i_enable         ( i_sw[0]     ) ,
      .i_signal_bit_in  ( PRBS9_out_Q ) ,
      .i_mapper_en      ( mapper_en   ) ,
      .o_conv           ( tx_out_Q    )
    ) ;

  slicer
    #()
    u_slicer_Q
    (
      .i_sample_sign  ( tx_out_Q[NB_TX_OUT-1] ) ,
      .o_bit          ( slicer_out_Q          )
    ) ;

  downsampler
    #()
    u_downsampler_Q
    (
      .i_sample       ( slicer_out_Q  ) ,
      .i_clock        ( i_clock       ) ,
      .i_reset        ( i_reset       ) ,
      .i_enable       ( PRBS9_enable  ) ,
      .i_phase_select ( i_sw[3:2]     ) ,
      .o_symb         ( symb_Q        )
    ) ;

  prbs_checker
    #()
    u_prbs_checker_Q
    (
      .i_clock    ( i_clock     ) ,
      .i_reset    ( ~i_reset    ) ,
      .i_enable   ( i_sw[1]     ) ,
      .i_ref_bit  ( PRBS9_out_Q ) ,
      .i_bit      ( symb_Q      ) ,
      .o_led      ( checker_Q   )
    ) ;

  // Output leds wiring

  assign o_led[0] = i_reset ;
  assign o_led[1] = i_sw[0] ;
  assign o_led[2] = i_sw[1] ;
  assign o_led[3] = checker_I & checker_Q ;
    
endmodule