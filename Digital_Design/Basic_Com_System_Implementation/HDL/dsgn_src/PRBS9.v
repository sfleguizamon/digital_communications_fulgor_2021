/*
 * .SYNOPSIS
 * <Overview of script>
 * .DESCRIPTION
 * <Brief description of script>
 * .PARAMETER <Parameter_Name>
 * <Brief description of parameter input required. Repeat this attribute if required>
 * .INPUTS
 * <Inputs if any, otherwise state None>
 * .OUTPUTS
 * <Outputs if anything is generated>
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Wednesday, August 4th 2021, 3:40:43 pm
 *    File: PRBS9.v
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://gitlab.com/sfleguizamon
 */

module PRBS9 #(
  parameter seed = 9'h1AA
) (
  input           i_clock   ,
  input           i_reset   ,
  input           i_enable  ,
  output          o_PRBS9
);

localparam NBPRBS9 = 9  ;

reg [NBPRBS9 - 1 : 0] regs  ;

always @(posedge i_clock or posedge i_reset) begin
  if (i_reset) begin
    regs  <= seed ;
  end
  else if (i_enable) begin
    regs  <= {regs[NBPRBS9-2:0], regs[NBPRBS9-5] ^ regs[NBPRBS9-1]} ;
  end
end

assign o_PRBS9 = regs[NBPRBS9-1]  ;

endmodule