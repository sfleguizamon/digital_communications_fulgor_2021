# Basic Communications System Verilog Description

En este proyecto se realizó la descripción de hardware (RTL) de un sistema de comunicaciones básicos, utilizando Verilog. El sistema consiste en el diagrama en bloques que se presenta a continuación.

![bloques sistema](blocks.svg)

Previo a la descripción, se realizaron simulaciones de alto nivel para corroborar el funcionamiento del sistema, y generar los coeficientes del filtro interpolador utilizado en el transmisor del sistema.

## PRBS9

Como semilla del transmisor, se utilizó una secuencia pseudo-aleatoria de bits (PRBS) de orden 9, lo que implica que la secuencia tiene 2^9-1 combinaciones de salida posibles antes de repetirse nuevamente.
La arquitectura del generador de PRBS de orden 9 se presenta a continuación.
![](HDL/schematics/PRBS9.svg)
## Simulación del sistema transceptor en alto nivel

Se realizó la simulación del sistema en un [script](Scripts/QPSK_transceiver_elemental.py) de Python 3. El mismo simula los distintos bloques del sistema, y además genera [gráficos](Scripts/plots/) y [reportes](Scripts/reports/) para contrastar con los resultados de la simulación del RTL.

### Generación de la semilla del transmisor

Para la generación de la PRBS9 en el simulador de alto nivel, se utilizó un [modelo de flip flop](Scripts/tool/ff_model.py) provisto por Fundación Fulgor, que permitió emular el comportamiento del RTL propio de la generación de una PRBS9.

Este generador de PRBS9 se logra con un registro de desplazamiento de 9 bits, cuya entrada es la operación XOR entre la salida del último registro y la salida del quinto. La salida se toma secuencialmente del último registro.

### Generación y cuantización de los coeficientes del filtro

Otro paso de simulación consiste en la generación de los coeficientes del filtro Raised Cosine necesario para filtrar los símbolos transmitidos. Además, estos coeficientes fueron cuantizados utilizando representación de punto fijo S(8, 7). La respuesta al impulso del sistema, se presenta a continuación. Se puede observar allí mismo el efecto de la cuantización de los coeficientes.

![](Scripts/plots/Impulse_responses_fxd_S(8-7).svg)

También se presentan las respuestas en frecuencia de ambos casos.

![](Scripts/plots/Frequency_responses_fxd_S(8-7).svg)
### Filtrado de la información

Para el filtrado de los bits generados, primero debió realizarse el mapeo de los mismos a los símbolos utilizados. Una vez realizado esto, se realiza un sobremuestreo de la señal (padding) para luego convolucionarla con el filtro.

El proceso de sobremuestreo y convolución fue simulado utilizando el [modelo de flip flop](Scripts/tool/ff_model.py) mencionado, representando la descomposición polifásica de un filtro FIR. Este proceso fue de gran ayuda a la hora de comprender el funcionamiento del hardware a describir en Verilog.

De acuerdo con los requisitos solicitados (tasa de sobremuestreo = 4 y filtro de 24 taps), la arquitectura del filtro FIR utilizado fue:

![](HDL/schematics/FIR.svg)

Las señales filtradas, se presentan a continuación.

#### In-phase signal component
![](Scripts/plots/Tx_out_I.svg)
![](Scripts/plots/Tx_out_eye_I.svg)

#### Quadrature signal component
![](Scripts/plots/Tx_out_Q.svg)
![](Scripts/plots/Tx_out_eye_Q.svg)

### Diezmado

Debido a que estamos considerando interferencia inter-símbolo (ISI) nula, y que no existe ruido añadido, el receptor sólo consiste en un downsampler, es decir en un diezmador. Si la fase de muestreo es correcta, la constelación recibida es correcta.

Las constelaciones recibidas para las distintas posibles fases de muestreos se presentan en la siguiente figura.

![](Scripts/plots/Rx_constellation_diagrams.svg)

---
## RTL en Verilog

La descripción del sistema se realizó en etapas de la misma manera que la simulación. 
### PRBS9

El generador de PRBS9 se realizó en el módulo [PRBS9](HDL/dsgn_src/PRBS9.v). Básicamente consiste en un registro de desplazamiento, cuya entrada es la XOR entre la salida del registro 9 y la salida del registro 5, como puede observaese a continuación.
![](HDL/schematics/PRBS9_sch.svg)

Se realizón un [testbench](HDL/simu_src/tb_PRBS9.v) de este módulo, resultando la secuencia que se presenta a continuación.

![](HDL/simu_src/wfgs/PRBS9.png)

### FIR Filter

La arquitectura del Filtro FIR se describió en el módulo [FIR_Filter](HDL/dsgn_src/FIR_Filter.v) y se realizó un [testbench](HDL/simu_src/tb_FIR_Filter.v), que consistía en excitar la entrada con un impulso, para observar la respuesta al impulso del filtro, como se puede observar a continuación.

![](HDL/simu_src/wfgs/FIR_sim.png)

Finalmente, el hardware inferido por la descripción resultó en el esquemático que se presenta a continuación.

![](HDL/schematics/FIRsch.svg).
### Slicer and downsampler

Debido a que la ISI producida por el transmisor es nula, que no se añade ruido a la señal en el canal, ni tampoco existe error de frecuencia o fase en el receptor, en el receptor la señal sólo debe ser pasada por un [slicer](HDL/dsgn_src/slicer.v) y un [downsampler](HDL/dsgn_src/downsampler.v) a fin de revertir los efector del mapper y del upsampling generado por el filtro.

El [slicer](HDL/dsgn_src/slicer.v) consiste en un inversor, que recibe el signo de la muestra y devuelve 0 o 1 dependiendo de que el valor de la muestra se trate de un valor negativo o positivo, respectivamente.

Para el caso del [downsampler](HDL/dsgn_src/downsampler.v), se describió un registro de desplazamiento que va acumulando muestras y que, de acuerdo a la fase seleccionada, devuelve una muestra determinada de las acumuladas.

![](HDL/schematics/downsampler_sch.svg)
### PRBS Checker

Se utilizó un [módulo](HDL/dsgn_src/prbs_checker.v), provisto por Fundación Fulgor, que compara la PRBS9 generada con la recibida en el receptor y detecta cuando la Bit-Error Rate es nula. Además posee un contador de errores, para el caso de BER no nula.

### Control

El módulo [control](HDL/dsgn_src/control.v) tiene dos fuciones. En primer lugar, habilitar el mapper. En segundo lugar, debido a que utilizamos una sola señal de clock, es necesario sincronizar los módulos puesto que la frecuencia a la que debe trabajar internamente el filtro debe ser 'oversampling rate' veces mayor que la frecuencia de generación de la señal de entrada. Es por ello que este módulo se encarga de habilitar la generación de PRBS9, teniendo en cuenta lo expuesto.
### TOP
Finalmente, se instanciaron todos los módulos en el módulo [top](HDL/dsgn_src/top.v). Se realizaron dos instancias para todos los casos a excepción del módulo de control. Esto es así porque se deseaba generar la arquitectura correspondiente al procesamiento de la señal en fase (generalmente denotada con I), y la correspondiente al procesamiento de la señal en cuadratura (denotada con Q). El resultado se presenta a continuación.

![](HDL/schematics/top_sch.svg)

Por otra parte, se realizó un [testbench](HDL/simu_src/tb_top.v) del sistema general, en el que se pueden observar la evolución de todas las señales de salida del [diagrama en bloques](system_blocks.png) presentado al comienzo.

Los switches [0] y [1] activan el transmisor y receptor, respectivamente. Los restantes son utilizados para controlar la fase de muestreo del downsampler.

Respecto de las salidas, los leds [0], [1] y [2] están conectados a la entrada de reset, TxEnable y RxEnable, respectivamente. El led restante es el que indica cuando la Bit-Error Rate es nula.

Además de las señales mencionadas, se presentan las señales de salida del transmisor y los contadores del módulo [prbs_checker](HDL/dsgn_src/prbs_checker.v).

![](HDL/simu_src/wfgs/top_sim.png)
