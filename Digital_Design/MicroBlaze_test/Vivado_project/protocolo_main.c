/*
 * .SYNOPSIS
 * This program controls GPIOs of MicroBlaze microcontroller depending on the data received 
 * through UART. It also reports the status of some GPIOs. 
 * .DESCRIPTION
 * Received frame is 2 bytes in size, and it's composed as it follows:
 *   | MODE | LED1 | LED1 | LED2 | LED3 | LED4 |
 *   - MODE (4bits): 
 *       0x0 -> write LEDs
 *       0xF -> read switches status
 *   - LED1 to LED4 (3bits each field):
 *     Depends on desired colours (RGB combinations).
 *       BGR    Result
 *       100 -> Blue
 *       010 -> Green
 *       110 -> Cyan
 *       001 -> Red
 *       101 -> Magenta
 *       011 -> Yellow
 *       111 -> White
 * .PARAMETER <Parameter_Name>
 * None
 * .INPUTS
 * None
 * .OUTPUTS
 * None
 * .NOTES
 *    Version:        0.1
 *    Author:         Santiago F. Leguizamon
 *    Email:          stgoleguizamon@gmail.com
 *    Creation Date:  Friday, August 20th 2021, 12:55:49 pm
 *    File: protocolo_main.c
 * HISTORY:
 * Date      	          By	Comments
 * ----------	          ---	----------------------------------------------------------
 * 
 * .LINK
 *    https://github.com/sfleguizamon
 * 
 */

#include <stdio.h>
#include <string.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xgpio.h"
#include "platform.h"
#include "xuartlite.h"
#include "microblaze_sleep.h"

#define PORT_IN	 		XPAR_AXI_GPIO_0_DEVICE_ID //XPAR_GPIO_0_DEVICE_ID
#define PORT_OUT 		XPAR_AXI_GPIO_0_DEVICE_ID //XPAR_GPIO_0_DEVICE_ID

//Device_ID Operaciones
#define def_SOFT_RST            0
#define def_ENABLE_MODULES      1
#define def_LOG_RUN             2
#define def_LOG_READ            3

XGpio GpioOutput;
XGpio GpioInput;
XUartLite uart_module;

int main()
{
	init_platform();

	int Status;
    u32 GPIOvalue;
    unsigned char sdata;
    u8 rdata[2];
    u16 leds_status;

	/* Peripherals initialization */

	XUartLite_Initialize(&uart_module, 0);

	Status=XGpio_Initialize(&GpioInput, PORT_IN);
	if(Status!=XST_SUCCESS){
        return XST_FAILURE;
    }

	Status=XGpio_Initialize(&GpioOutput, PORT_OUT);
	if(Status!=XST_SUCCESS){
		return XST_FAILURE;
	}

	XGpio_SetDataDirection(&GpioOutput, 1, 0x00000000); // GPIO de salida
	XGpio_SetDataDirection(&GpioInput, 1, 0xFFFFFFFF); // GPIO de entrada



    /* Super Loop */
	while(1){

        /* Reading UART */
        read(stdin,&rdata[0],2);

        /* Reading GPIO and sending through UART*/
        if(rdata[0] & 0xF0){
            GPIOvalue = XGpio_DiscreteRead(&GpioInput, 1);
            sdata=(char)(GPIOvalue&(0xF));
            while(XUartLite_IsSending(&uart_module)){}
            XUartLite_Send(&uart_module, &(sdata),1);
        }

        /* Writing GPIO (LEDs) */
        else{
            leds_status = rdata[0]<<8 | rdata[1];
            XGpio_DiscreteWrite(&GpioOutput,1, (u32) leds_status); 
        }

    }
	
	cleanup_platform();
	return 0;
}
