'''
.SYNOPSIS
This program allows to communicate and control our MicroBlaze microcontroller through UART. 
.DESCRIPTION
Transmitted frame is 2 bytes in size, and it's composed as it follows:
    | MODE | LED1 | LED1 | LED2 | LED3 | LED4 |
    - MODE (4bits): 
        0x0 -> write LEDs
        0xF -> read switches status
    - LED1 to LED4 (3bits each field):
      Depends on desired colours (RGB combinations).
        BGR    Result
        100 -> Blue
        010 -> Green
        110 -> Cyan
        001 -> Red
        101 -> Magenta
        011 -> Yellow
        111 -> White
.PARAMETER
It receives as argument the ttyUSB number connected to the FPGA.
.INPUTS
None
.OUTPUTS
None
.NOTES
   Version:        0.1
   Author:         Santiago F. Leguizamon
   Email:          stgoleguizamon@gmail.com
   Creation Date:  Tuesday, August 24th 2021, 1:20:22 pm
   File: Python_UART_FPGA.py
HISTORY:
Date      	          By	Comments
----------	          ---	----------------------------------------------------------

.LINK
   https://github.com/sfleguizamon

.COMPONENT
 Required Modules: 
    time, serial and sys.
'''

import time
import serial
import sys

def main():

    if len(sys.argv) > 2:
        print("WARNING: too many arguments entered.")
        return 1
    elif len(sys.argv) < 2:
        print("WARNING: not enough arguments entered. Using default serial port (ttyUSB5).")
        port = '/dev/ttyUSB5'
    else:
        port = '/dev/ttyUSB' + str(sys.argv[1])

    print("Using serial port {}." .format(port))

    ser = serial.Serial(
        port        = port                  ,	# Serial port configuration
        baudrate    = 115200                ,
        parity      = serial.PARITY_NONE    ,
        stopbits    = serial.STOPBITS_ONE   ,
        bytesize    = serial.EIGHTBITS
    )

    ser.isOpen()
    ser.timeout = 5

    Menu = """
    \rEnter LEDs status:
    \r 0: Disabled.
    \r 1: Enabled.

    \rSelect each LED colour by using the following letters:
    \r B: Blue
    \r G: Green
    \r C: Cyan
    \r R: Red 
    \r M: Magenta
    \r Y: Yellow
    \r W: White
    \r"""

    colourBits_dict = {
        'B': '100',
        'G': '010',
        'C': '110',
        'R': '001',
        'M': '101',
        'Y': '011',
        'W': '111'
    }

    while True:

        rw_selection = input("\nEnter 'r' to read vio input, 'w' to modify LEDs status or 'q' to quit.\n\r<< ")

        # Read switches

        if (rw_selection == 'r'):   
            bits_frame = '0b1111000000000000'
            frame = bytes([int(bits_frame[2:10], base = 2)])
            frame = frame + bytes([int(bits_frame[10:18], base = 2)]) 
            ser.write(frame)
            time.sleep(1)
            readData = ser.read(1)
            out = bin(int.from_bytes(readData,byteorder='big'))[2:].zfill(4)
            if out != '':
                print ("Switches status: " + out)

        # Write LEDs

        elif (rw_selection == 'w'):
            bits_frame = '0b0000'
            for i in range(4):
                if i == 0:
                    print(Menu)
                led_state = input("LED {} status: " .format(i+1))
                if(int(led_state)):
                    led_col = input("LED {} colour: " .format(i+1))
                    bits_frame = bits_frame + colourBits_dict[led_col]
                else:
                    bits_frame = bits_frame + '000'
            frame = bytes([int(bits_frame[2:10], base = 2)])
            frame = frame + bytes([int(bits_frame[10:18], base = 2)]) 
            ser.write(frame)
            time.sleep(1)

        # Exit

        elif (rw_selection == 'q'):
            ser.close()
            return 0

        else:
            print("ERROR: invalid option.")
            ser.close()
            return 1

if __name__ == "__main__":
    main()