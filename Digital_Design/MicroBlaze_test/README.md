# MicroBlaze testing platform

En este proyecto se realiza la instanciación de un microcontrolador embebido en la FPGA, y su programación. A través de un script de Python y utilizando puerto serie, la PC se comunica con el microcontrolador para controlar los GPIO del mismo, que se encuentran conectados a los LEDs y switches del kit de desarrollo Artix-7-35T.

---

# Script de Python

Para el proyecto, se realizó un [script](Vivado_project/Python_UART_FPGA.py) de Python que nos permite elegir el estado de los LEDs mencionados y su color, o leer el estado de los switches. En función de esto, el programa genera una trama de 2 bytes, que se envía a través del puerto serie.

El script recibe como argumento el número de puerto serie utilizado. Se presentan algunas capturas de pantalla de su funcionamiento a continuación.
![](Screenshots/script_capture.png).

La trama generada consiste de 2 bytes y se detalla a continuación:

| MODE | LED1 | LED1 | LED2 | LED3 | LED4 |

- MODE (4bits): indica si se requiere operación de lectura o escritura de los GPIO. 
    - 0x0 -> write LEDs
    - 0xF -> read switches status
- LED1 to LED4 (3bits each field): Indica el color de los LEDs cuyo estado sea encendido (1). Las posibles combinaciones RGB se detallan a continuación:
    - BGR    Result
    - 100 -> Blue
    - 010 -> Green
    - 110 -> Cyan
    - 001 -> Red
    - 101 -> Magenta
    - 011 -> Yellow
    - 111 -> White

---
# Firmware del microcontrolador
La [aplicación](Vivado_project/protocolo_main.c) principal del firmware del microcontrolador fue escrita en C. Consiste en un programa que inicializa los periféricos utilizados, y de acuerdo a la trama recibida por la UART, lee el estado de los switches y lo envía por UART; o escribe los GPIO correspondientes a los LEDs, de acuerdo al color y estado seleccionado. Se utilizó [Vitis](https://www.xilinx.com/products/design-tools/vitis.html) como plataforma de desarrollo de software.

---
# HDL (Verilog)
Se utilizó [Vivado](https://www.xilinx.com/products/design-tools/vivado.html) como plataforma. Allí se utilizó la herramienta IP Integrator para crear un diseño en bloques de IPs propios de la librería de Vivado. Se realizaron dos diseños de bloques, el del microcontrolador embebido y otro correspondiente a un módulo Virtual Input/Output (VIO), ya que la conexión a la FPGA se realizó de manera remota.

## MicroBlaze
Este diseño consiste en una IP de un microcontrolador MicroBlaze y sus periféricos asociados, como la UART y los GPIO.

![](Screenshots/MicroGPIO.png)
## Virtual Input/Output (VIO)
Como se mencionó, la conexión a la FPGA se realizó de manera remota. Por lo tanto se utilizó un módulo VIO para el control y monitoreo de las salidas de la FPGA.

![](Screenshots/vio.png)

La instanciación de los dos bloques generados y sus conexiones fue realizada en la descripción que se encuentra en el archivo [fpga_procom.v](Vivado_project/fpga_procom.v).

Finalmente, se realizó el test del sistema utilizando Hardware Manager de Vivado para controlar la VIO. Se envió la trama correspondiente a las configuraciones mostradas en la siguiente imágen:

![](Screenshots/script_capture.png)

El estado del VIO para dichas configuraciones se presenta a continuación:

![](Screenshots/vio_capture.png)

---
# Consideraciones & bugs a tener en cuenta
## Vitis
- En Run Configurations, por defecto está desactivado la compilación y linking del programa. Por lo tanto, hay que realizar estas tareas por separado, o desactivar la opción "Disable build before launch" dentro de Run Configurations.
- Al momento de cargar en la FPGA el bitstream desde Vitis, puede aparecer un error que se soluciona como se indica a continuación:
  En _ide/bitstream/file_name.mmi hay que borrar lo que hay antes de /microblaze_0, por ejemplo, la línea siguiente
~~~
<Processor Endianness="Little" InstPath="u_micro/microblaze_0">
~~~ 
quedaría como: 
~~~
<Processor Endianness="Little" InstPath="/microblaze_0"> 
~~~
---