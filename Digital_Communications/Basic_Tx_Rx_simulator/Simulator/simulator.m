% Pendientes
% TX:
% Agregar plots tx:
% *****************
%   - eyediagram
%   - plot transmitted_data.symbols y transmitted_data.signal
%   - plot pulse shaping filter frecuencia y tiempo
%   - constelacion transmitida
%   - PSD de los simbolos (antes y despues de filtrar) 
%   - histograma de los simbolos

% CH:
% Agregar plots channel: (acomodar ejes y realizar bien la PSD)
% **********************
%   - senal recibida (entrada sobre salida) 
%   - respuesta en frecuencia
%   - PSD
% RX:
% Calculos de BER, SER y EbNo
% Agregar plots rx:
% *****************
%   - histograma slicer in 
%   - Constelaciones slc in/out/txsymbs
%   - eyediagram?
%   -
%   -



clc; clear all; close all


% Transmitter %

tx_config_struct.M = 4;
tx_config_struct.BR = 16e9;
tx_config_struct.Lsymbs = 100e3;
tx_config_struct.os_rate = 4;
tx_config_struct.pulse_shaping_rolloff = 0.1;
tx_config_struct.pulse_shaping_Ntaps = 501;
tx_config_struct.pulse_shaping_type = 0;

tx = transmitter(tx_config_struct)
transmitted_data = tx.transmit;

%% Channel %

channel_config_struct.channel_type = 0; % 0: AWGN, 1: use transfer function, 2: tfunction + AWGN
channel_config_struct.SNR = 50;
channel_config_struct.impulse_response = [];

ch = channel(channel_config_struct)
channelized_signal = ch.run(transmitted_data.signal);

%% Transmited vs channel affected signal plots
figure
subplot(2, 1, 1)
plot(real(transmitted_data.signal), '-b', 'LineWidth', 1.5, 'DisplayName', "Transmitted signal")
hold on
plot(real(channelized_signal), '-r', 'LineWidth', 1.5, 'DisplayName', "Channelized signal")
xlim([350, 400]); title("Real part"); legend(); grid on

subplot(2, 1, 2)
plot(imag(transmitted_data.signal), '-b', 'LineWidth', 1.5, 'DisplayName', "Transmitted signal")
hold on
plot(imag(channelized_signal), '-r', 'LineWidth', 1.5, 'DisplayName', "Channelized signal")
xlim([350, 400]); title("Imaginary part"); legend(); grid on

%% Channel frequency response plot
NFFT = 8*1024;
H = fft(ch.impulse_response, NFFT);

figure

subplot(2, 1, 1)
plot(20*log10(abs(H)), '-b', 'LineWidth', 1.5, 'DisplayName', "Freq response (mod)")
xlim([0, NFFT]); legend(); grid on; title("Channel frequency response")
subplot(2, 1, 2)
plot(angle(H), '-r', 'LineWidth', 1.5, 'DisplayName', "Freq response (phase)")
xlim([0, NFFT]); legend(); grid on;

%% Noise PSD
% [Pxx,W] = pwelch(ch.genNoise(var(transmitted_data.signal), size(transmitted_data.signal)));
% figure
% plot(W, Pxx)
% xlim([0, W(end)]); ylim([-0.01, 0.01]); grid on

%% Receptor %

receptor_config_struct.M = tx.M;
receptor_config_struct.BR = tx.BR;
receptor_config_struct.os_rate = tx.os_rate;
receptor_config_struct.samplingRate = tx.samplingRate;

receptor_config_struct.matched_filter_rolloff  = 0.1;  % Pulse-shaping filter rolloff
receptor_config_struct.matched_filter_Ntaps    = 500;  % Number of pulse-shaping filter taps  
receptor_config_struct.matched_filter_type     = 0;    % 0 = RRC, 1 = RC

receptor_config_struct.phase_error = 0;
receptor_config_struct.freq_error = 0;
receptor_config_struct.phase = 0;

rx = receiver(receptor_config_struct)
received_data = rx.receive(channelized_signal);
% Eyediagram
eyediagram(received_data.slicer_in(20000:end-20000), rx.M)
% Histograma slicer in
figure
subplot(2, 1, 1)
histogram(real(received_data.slicer_in), 100)
subplot(2, 1, 2)
histogram(imag(received_data.slicer_in), 100)
% Constelaciones slc in/out/txsymbs
figure
scatter(real(received_data.slicer_in), imag(received_data.slicer_in), '*k', 'LineWidth', 1.5, 'DisplayName', "Slicer input")
hold on
scatter(real(transmitted_data.symbols), imag(transmitted_data.symbols), 'xb', 'LineWidth', 1.5, 'DisplayName', "Transmitted symbols")
scatter(real(received_data.slicer_out), imag(received_data.slicer_out), 'or', 'LineWidth', 1.5, 'DisplayName', "Slicer output")
legend(); grid on