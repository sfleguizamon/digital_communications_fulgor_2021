%.SYNOPSIS
%<Overview of script>
%.DESCRIPTION
%<Brief description of script>
%.PARAMETER <Parameter_Name>
%<Brief description of parameter input required. Repeat this attribute if required>
%.INPUTS
%<Inputs if any, otherwise state None>
%.OUTPUTS
%<Outputs if anything is generated>
%.NOTES
%   Version:        0.1
%   Author:         Santiago F. Leguizamon
%   Email:          stgoleguizamon@gmail.com
%   Creation Date:  Wednesday, July 21st 2021, 1:09:02 pm
%   File: transmitter.m
%   Copyright (c) 2021 Santiago F. Leguizamon
%HISTORY:
%Date      	          By	Comments
%----------	          ---	----------------------------------------------------------
%
%.LINK
%   https://github.com/sfleguizamon

classdef transmitter
    properties (GetAccess = public, SetAccess = private)
        M                       {mustBePositive, mustBeReal}                                                        = 4;        % modulation levels
        BR                      {mustBePositive, mustBeReal}                                                        = 16e9;     % Baud rate
        Lsymbs                  {mustBePositive, mustBeReal}                                                        = 100e3;    % Number of symbols to be generated
        os_rate                 {mustBePositive, mustBeReal}                                                        = 4;        % Oversampling rate
        pulse_shaping_rolloff   {mustBeNonnegative, mustBeReal, mustBeLessThanOrEqual(pulse_shaping_rolloff, 1)}    = 0.2;      % Pulse-shaping filter rolloff
        pulse_shaping_Ntaps     {mustBePositive, mustBeReal}                                                        = 200;      % Number of pulse-shaping filter taps  
        pulse_shaping_type      {mustBeNonnegative, mustBeReal}                                                     = 0;        % 0 = RRC, 1 = RC
    end
    properties (Dependent)

    end
    methods
        % Constructor method %
        % Constructor receives a struct of configurations. In case struct was not
        % received as constructor parameter, default values are used.
        function obj = transmitter(tx_config)
            if nargin == 0
                fprintf('Transmitter was created using default parameters.')
            else
                tx_conf_fields = fieldnames(tx_config)
                for i = 1:length(tx_conf_fields)
                    switch (tx_conf_fields{i})
                    case 'M'
                        obj.M = tx_config.(tx_conf_fields{i});
                    case 'BR'
                        obj.BR = tx_config.(tx_conf_fields{i});
                    case 'Lsymbs'
                        obj.Lsymbs = tx_config.(tx_conf_fields{i});
                    case 'os_rate'
                        obj.os_rate = tx_config.(tx_conf_fields{i});
                    case 'pulse_shaping_rolloff'
                        obj.pulse_shaping_rolloff = tx_config.(tx_conf_fields{i});
                    case 'pulse_shaping_Ntaps'
                        obj.pulse_shaping_Ntaps = tx_config.(tx_conf_fields{i});
                    case 'pulse_shaping_type'
                        obj.pulse_shaping_type = tx_config.(tx_conf_fields{i});
                    otherwise   % In case field does not exist as a class property.
                        error("Field '%s' does not exist", tx_conf_fields{i})
                    end
                end
            end
        end
        
        % Methods %
        function value = timeBetweenSymbols(obj)
            value = 1/obj.BR;
        end
        function value = samplingRate(obj)
            value = obj.os_rate*obj.BR;
        end
        function value = samplingPeriod(obj)
            value = 1/obj.samplingRate;
        end
        function value = bitsPerSymbol(obj)
            value = log2(obj.M);
        end

        function [taps] = pulse_shaping_filter(obj)
            if obj.pulse_shaping_type == 0
                filter_type = 'sqrt';
            else
                filter_type = 'normal';
            end
            zero_crossings = (obj.pulse_shaping_Ntaps-1)/(obj.os_rate * 2);
            zero_crossings = round(zero_crossings, 0); % Number of zero crossings must be integer
            taps = rcosine(obj.BR, obj.samplingRate, filter_type, obj.pulse_shaping_rolloff, zero_crossings);
        end

        function tx_output = transmit(obj)
            data = randi([0,1], obj.Lsymbs, obj.bitsPerSymbol); % Generating random bits with an uniform distribution.
            data_Symbols = bi2de(data);
            clear data; % Freeing memory in case Lsymbs is high
            tx_output.symbols = qammod(data_Symbols, obj.M, 'gray');
            clear data_Symbols;
            xup = upsample(tx_output.symbols, obj.os_rate);
            tx_output.signal = filter(obj.pulse_shaping_filter, 1, xup);
            clear xup;
        end

    end

end