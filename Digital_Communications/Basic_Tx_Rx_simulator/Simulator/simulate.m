% Pendientes
% TX:
% Agregar plots tx:
% *****************
%   - eyediagram
%   - plot transmitted_data.symbols y transmitted_data.signal
%   - plot pulse shaping filter frecuencia y tiempo
%   - constelacion transmitida
%   - PSD de los simbolos (antes y despues de filtrar) 
%   - histograma de los simbolos

% CH:
% Agregar plots channel: (acomodar ejes y realizar bien la PSD)
% **********************
%   - senal recibida (entrada sobre salida) 
%   - respuesta en frecuencia
%   - PSD
% RX:
% Calculos de BER, SER y EbNo
% Agregar plots rx:
% *****************
%   - histograma slicer in 
%   - Constelaciones slc in/out/txsymbs
%   - eyediagram?
%   -
%   -

function output = simulate(sim_config)
%myFun - Description
%
% Syntax: output = myFun(input)
%
% Long description
    if nargin == 0
        fprintf('Running simulation using default parameters.')
        tx = transmitter()
        transmitted_data = tx.transmit;
        ch = channel()
        channelized_signal = ch.run(transmitted_data.signal);
        rx = receiver()
        received_data = rx.receive(channelized_signal);
    else
        tx = transmitter(sim_config.tx)
        transmitted_data = tx.transmit;
        ch = channel(sim_config.ch)
        channelized_signal = ch.run(transmitted_data.signal);
        rx = receiver(sim_config.rx)
        received_data = rx.receive(channelized_signal);
    end

    % Transmitter & Channel%

    % Transmited vs channel affected signal plots
    figure
    subplot(2, 1, 1)
    plot(real(transmitted_data.signal), '-b', 'LineWidth', 1.5, 'DisplayName', "Transmitted signal")
    hold on
    plot(real(channelized_signal), '-r', 'LineWidth', 1.5, 'DisplayName', "Channelized signal")
    xlim([350, 400]); title("Real part"); legend(); grid on

    subplot(2, 1, 2)
    plot(imag(transmitted_data.signal), '-b', 'LineWidth', 1.5, 'DisplayName', "Transmitted signal")
    hold on
    plot(imag(channelized_signal), '-r', 'LineWidth', 1.5, 'DisplayName', "Channelized signal")
    xlim([350, 400]); title("Imaginary part"); legend(); grid on

    % Channel frequency response plot
    NFFT = 8*1024;
    H = fft(ch.impulse_response, NFFT);

    figure
    subplot(2, 1,    1)
    plot(20*log10(abs(H)), '-b', 'LineWidth', 1.5, 'DisplayName', "Freq response (mod)")
    xlim([0, NFFT]); legend(); grid on; title("Channel frequency response")
    subplot(2, 1, 2)
    plot(angle(H), '-r', 'LineWidth', 1.5, 'DisplayName', "Freq response (phase)")
    xlim([0, NFFT]); legend(); grid on;

    % Noise PSD
    % [Pxx,W] = pwelch(ch.genNoise(var(transmitted_data.signal), size(transmitted_data.signal)));
    % figure
    % plot(W, Pxx)
    % xlim([0, W(end)]); ylim([-0.01, 0.01]); grid on

    % Receptor plots%

    % Eyediagram
    eyediagram(received_data.slicer_in(20000:end-20000), rx.M)
    % Histograma slicer in
    figure
    subplot(2, 1, 1)
    histogram(real(received_data.slicer_in), 100)
    subplot(2, 1, 2)
    histogram(imag(received_data.slicer_in), 100)
    % Constelaciones slc in/out/txsymbs
    figure
    scatter(real(received_data.slicer_in), imag(received_data.slicer_in), '*k', 'LineWidth', 1.5, 'DisplayName', "Slicer input")
    hold on
    scatter(real(received_data.slicer_out), imag(received_data.slicer_out), 'or', 'LineWidth', 1.5, 'DisplayName', "Slicer output")
    scatter(real(transmitted_data.symbols), imag(transmitted_data.symbols), 'xb', 'LineWidth', 1.5, 'DisplayName', "Transmitted symbols")
    legend(); grid on

end