% Agregar factor de escala al limite
function [slicer_out] = slicer_QAM_M(slicer_input, M)
%SLICER_QAM_M Summary of this function goes here
%   Detailed explanation goes here
for i = 1:length(slicer_input)

    if M==4
        if real(slicer_input(i)) > 0
            real_a_hat(i) = 1;
        else
            real_a_hat(i) = -1;
        end
        if imag(slicer_input(i)) > 0
            imag_a_hat(i) = 1;
        else
            imag_a_hat(i) = -1;
        end

    elseif M==16
        if real(slicer_input(i)) > 0 && real(slicer_input(i)) > 2
            real_a_hat(i) = 3;
        elseif real(slicer_input(i)) > 0 && real(slicer_input(i)) < 2
            real_a_hat(i) = 1;
        elseif real(slicer_input(i)) < 0 && real(slicer_input(i)) < -2
            real_a_hat(i) = -3;
        else
            real_a_hat(i) = -1;
        end
        if imag(slicer_input(i)) > 0 && imag(slicer_input(i)) > 2
            imag_a_hat(i) = 3;
        elseif imag(slicer_input(i)) > 0 && imag(slicer_input(i)) < 2
            imag_a_hat(i) = 1;
        elseif imag(slicer_input(i)) < 0 && imag(slicer_input(i)) < -2
            imag_a_hat(i) = -3;
        else
            imag_a_hat(i) = -1;
        end
    
    elseif M==64
        if real(slicer_input(i)) > 6
            real_a_hat(i) = 7;
        elseif real(slicer_input(i)) > 4 && real(slicer_input(i)) < 6
            real_a_hat(i) = 5;
        elseif real(slicer_input(i)) > 2 && real(slicer_input(i)) < 4
            real_a_hat(i) = 3;
        elseif real(slicer_input(i)) > 0 && real(slicer_input(i)) < 2
            real_a_hat(i) = 1;
        elseif real(slicer_input(i)) < -6
            real_a_hat(i) = -7;
        elseif real(slicer_input(i)) < -4 && real(slicer_input(i)) > -6
            real_a_hat(i) = -5;
        elseif real(slicer_input(i)) < -2 && real(slicer_input(i)) > -4
            real_a_hat(i) = -3;
        else
            real_a_hat(i) = -1;
        end
        if imag(slicer_input(i)) > 6
            imag_a_hat(i) = 7;
        elseif imag(slicer_input(i)) > 4 && imag(slicer_input(i)) < 6
            imag_a_hat(i) = 5;
        elseif imag(slicer_input(i)) > 2 && imag(slicer_input(i)) < 4
            imag_a_hat(i) = 3;
        elseif imag(slicer_input(i)) > 0 && imag(slicer_input(i)) < 2
            imag_a_hat(i) = 1;
        elseif imag(slicer_input(i)) < -6
            imag_a_hat(i) = -7;
        elseif imag(slicer_input(i)) < -4 && imag(slicer_input(i)) > -6
            imag_a_hat(i) = -5;
        elseif imag(slicer_input(i)) < -2 && imag(slicer_input(i)) > -4
            imag_a_hat(i) = -3;
        else
            imag_a_hat(i) = -1;
        end
    else
        error("Slicer for M = %d is not implemented", M)
    end
end

slicer_out = real_a_hat + 1j*imag_a_hat;

end