clc; clear all; close all

sim_config.tx.M = 64;
sim_config.tx.BR = 16e9;
sim_config.tx.Lsymbs = 100e3;
sim_config.tx.os_rate = 4;
sim_config.tx.pulse_shaping_rolloff = 0.1;
sim_config.tx.pulse_shaping_Ntaps = 501;
sim_config.tx.pulse_shaping_type = 0;

sim_config.ch.channel_type = 0; % 0: AWGN, 1: use transfer function, 2: tfunction + AWGN
sim_config.ch.SNR = 500;
sim_config.ch.impulse_response = [];

sim_config.rx.M = sim_config.tx.M;
sim_config.rx.BR = sim_config.tx.BR;
sim_config.rx.os_rate = sim_config.tx.os_rate;
sim_config.rx.samplingRate = sim_config.tx.BR*sim_config.tx.os_rate;
sim_config.rx.matched_filter_rolloff  = 0.1;  % Pulse-shaping filter rolloff
sim_config.rx.matched_filter_Ntaps    = 500;  % Number of pulse-shaping filter taps  
sim_config.rx.matched_filter_type     = 0;    % 0 = RRC, 1 = RC
sim_config.rx.phase_error = 0;
sim_config.rx.freq_error = 0;
sim_config.rx.phase = 0;

simulate(sim_config)
