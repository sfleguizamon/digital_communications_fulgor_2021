clc; clear all; close all;

EbNo_dB = 0.5:0.1:10.5;
EbNo = 10.^(EbNo_dB/10);
M = 4;

Pb = 4/log2(M) * ( 1-2^(-log2(M)/2)) * 0.5 * erfc( sqrt((3*log2(M)*EbNo)/(M-1)) / sqrt(2) ); % Bit-error probability
ber_MATLAB = berawgn(EbNo_dB, 'qam', 4);

plot(EbNo_dB, ber_MATLAB, '-r','LineWidth', 1.5,'DisplayName', "berawgn()")
hold on
ylabel("BER", 'Interpreter', 'latex')
xlabel("EbNo [dB]", 'Interpreter', 'latex')
legend('Interpreter', 'latex')
grid on






plot(EbNo_dB, Pb, '--b', 'LineWidth', 1.5, 'DisplayName', "Bit-error probability $P_b$")