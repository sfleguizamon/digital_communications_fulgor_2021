classdef channel
    properties (GetAccess = public, SetAccess = private)
        channel_type {mustBeNonnegative, mustBeReal, mustBeLessThanOrEqual(channel_type, 2)} = 0;   % 0: AWGN, 1: use transfer function, 2: transfer function and then AWGN
        SNR {mustBePositive, mustBeReal} = 40;
        impulse_response {mustBeNumeric} = [1];
    end
    methods
        % Constructor method %
        % Constructor receives a struct of configurations. In case struct was not
        % received as constructor parameter, default values are used.
        function obj = channel(channel_config)
            if nargin == 0
                fprintf('Channel was created using default parameters.')
            else
                ch_conf_fields = fieldnames(channel_config)
                for i = 1:length(ch_conf_fields)
                    switch (ch_conf_fields{i})
                    case 'channel_type'
                        obj.channel_type = channel_config.(ch_conf_fields{i});
                    case 'SNR'
                        obj.SNR = channel_config.(ch_conf_fields{i});
                    case 'impulse_response'
                        if ~isempty(channel_config.impulse_response)
                            obj.impulse_response = channel_config.(ch_conf_fields{i});
                        end
                    otherwise   % In case field does not exist as a class property.
                        error("Field '%s' does not exist", ch_conf_fields{i})
                    end
                end
            end
        end
        
        % Methods %
        function [output] = genNoise(obj, signal_power, signal_lenght)  % Complex & gaussian noise generation
            noise_power = signal_power/obj.SNR;
            No = noise_power;
            noise_real = sqrt(No/2)*randn(signal_lenght);
            noise_imag = sqrt(No/2)*randn(signal_lenght);
            output = noise_real + 1j*noise_imag;
        end

        function [output] = channelConvolve(obj, signal)    % Convolve transmmited signal with channel response
            output = filter(obj.impulse_response, 1, signal);
        end

        function [output] = run(obj, signal)
            switch obj.channel_type
            case 0  % AWGN
                signal_power = var(signal);
                signal_lenght = size(signal);
                output = signal + obj.genNoise(signal_power, signal_lenght);
            case 1  % Convolves with impulse response
                output = obj.channelConvolve(signal);
            case 2  % Convolves with impulse response + AWGN
                signal_power = var(signal);
                signal_lenght = size(signal);
                output = obj.channelConvolve(signal) + obj.genNoise(signal_power, signal_lenght);
            otherwise
                error("Undefined channel type.")
            end
        end

    end

end