classdef receiver
    properties (GetAccess = public, SetAccess = private)
        M               {mustBePositive, mustBeReal} = 4;
        BR              {mustBePositive, mustBeReal} = 16e9;
        os_rate         {mustBePositive, mustBeReal} = 4;
        samplingRate    {mustBePositive, mustBeReal} = 4*16e9;

        matched_filter_rolloff  {mustBeNonnegative, mustBeReal, mustBeLessThanOrEqual(matched_filter_rolloff, 1)} = 0.1;  % Pulse-shaping filter rolloff
        matched_filter_Ntaps    {mustBePositive, mustBeReal} = 200;  % Number of pulse-shaping filter taps  
        matched_filter_type     {mustBeNonnegative, mustBeReal} = 0;    % 0 = RRC, 1 = RC

        phase_error {mustBeNonnegative, mustBeReal} = 0;
        freq_error  {mustBeNonnegative, mustBeReal} = 0;
        phase       {mustBeNonnegative, mustBeReal} = 0;
    end
    methods
        function obj = receiver(rx_config)
            if nargin == 0
                fprintf('Receiver was created using default parameters.')
            else           
                rx_conf_fields = fieldnames(rx_config)
                for i = 1:length(rx_conf_fields)
                    switch (rx_conf_fields{i})
                    case 'M'
                        obj.M = rx_config.(rx_conf_fields{i});
                    case 'BR'
                        obj.BR = rx_config.(rx_conf_fields{i});
                    case 'os_rate'
                        obj.os_rate = rx_config.(rx_conf_fields{i});
                    case 'samplingRate'
                        obj.samplingRate = rx_config.(rx_conf_fields{i});
                    case 'matched_filter_rolloff'
                        obj.matched_filter_rolloff = rx_config.(rx_conf_fields{i});
                    case 'matched_filter_Ntaps'
                        obj.matched_filter_Ntaps = rx_config.(rx_conf_fields{i});
                    case 'matched_filter_type'
                        obj.matched_filter_type = rx_config.(rx_conf_fields{i});
                    case 'phase_error'
                        obj.phase_error = rx_config.(rx_conf_fields{i});
                    case 'freq_error'
                        obj.freq_error = rx_config.(rx_conf_fields{i});
                    case 'phase'
                        obj.phase = rx_config.(rx_conf_fields{i});
                    otherwise   % In case field does not exist as a class property.
                        error("Field '%s' does not exist", rx_conf_fields{i})
                    end
                end
            end
        end

        function [taps] = matched_filter(obj)
            if obj.matched_filter_type == 0
                filter_type = 'sqrt';
            else
                filter_type = 'normal';
            end
            zero_crossings = (obj.matched_filter_Ntaps-1)/(obj.os_rate * 2);
            zero_crossings = round(zero_crossings, 0); % Number of zero crossings must be integer
            taps = rcosine(obj.BR, obj.samplingRate, filter_type, obj.matched_filter_rolloff, zero_crossings);
            taps = conj(taps(end:-1:1)); % Just making explicit the process of making Matched Filter
        end

        function received_data = receive(obj, signal)
            signal = filter(obj.matched_filter, 1, signal);
            t = [0:length(signal)-1].' .* 1/obj.samplingRate;

            ep = exp(1j*2*pi*obj.freq_error.*t + 1j*obj.phase_error) ;
            g = signal .* ep ;

            received_data.slicer_in = g(obj.phase+1:obj.os_rate:end) ;
            received_data.slicer_out = slicer_QAM_M(received_data.slicer_in, obj.M);

        end
    end
end